import java.util.ArrayList;

public class StudentNetworkSimulator extends NetworkSimulator
{
    /*
     * Predefined Constants (static member variables):
     *
     *   int MAXDATASIZE : the maximum size of the Message data and
     *                     Packet payload
     *
     *   int A           : a predefined integer that represents entity A
     *   int B           : a predefined integer that represents entity B
     *
     *
     * Predefined Member Methods:
     *
     *  void stopTimer(int entity): 
     *       Stops the timer running at "entity" [A or B]
     *  void startTimer(int entity, double increment): 
     *       Starts a timer running at "entity" [A or B], which will expire in
     *       "increment" time units, causing the interrupt handler to be
     *       called.  You should only call this with A.
     *  void toLayer3(int callingEntity, Packet p)
     *       Puts the packet "p" into the network from "callingEntity" [A or B]
     *  void toLayer5(int entity, String dataSent)
     *       Passes "dataSent" up to layer 5 from "entity" [A or B]
     *  double getTime()
     *       Returns the current time in the simulator.  Might be useful for
     *       debugging.
     *  void printEventList()
     *       Prints the current event list to stdout.  Might be useful for
     *       debugging, but probably not.
     *
     *
     *  Predefined Classes:
     *
     *  Message: Used to encapsulate a message coming from layer 5
     *    Constructor:
     *      Message(String inputData): 
     *          creates a new Message containing "inputData"
     *    Methods:
     *      boolean setData(String inputData):
     *          sets an existing Message's data to "inputData"
     *          returns true on success, false otherwise
     *      String getData():
     *          returns the data contained in the message
     *  Packet: Used to encapsulate a packet
     *    Constructors:
     *      Packet (Packet p):
     *          creates a new Packet that is a copy of "p"
     *      Packet (int seq, int ack, int check, String newPayload)
     *          creates a new Packet with a sequence field of "seq", an
     *          ack field of "ack", a checksum field of "check", and a
     *          payload of "newPayload"
     *      Packet (int seq, int ack, int check)
     *          chreate a new Packet with a sequence field of "seq", an
     *          ack field of "ack", a checksum field of "check", and
     *          an empty payload
     *    Methods:
     *      boolean setSeqnum(int n)
     *          sets the Packet's sequence field to "n"
     *          returns true on success, false otherwise
     *      boolean setAcknum(int n)
     *          sets the Packet's ack field to "n"
     *          returns true on success, false otherwise
     *      boolean setChecksum(int n)
     *          sets the Packet's checksum to "n"
     *          returns true on success, false otherwise
     *      boolean setPayload(String newPayload)
     *          sets the Packet's payload to "newPayload"
     *          returns true on success, false otherwise
     *      int getSeqnum()
     *          returns the contents of the Packet's sequence field
     *      int getAcknum()
     *          returns the contents of the Packet's ack field
     *      int getChecksum()
     *          returns the checksum of the Packet
     *      int getPayload()
     *          returns the Packet's payload
                *
     *
     */


    // Alternating bit protocol
    // Send one packet, wait for the ack, send the next packet, ...

    // Add any necessary class variables here.  Remember, you cannot use
    // these variables to send messages error free!  They can only hold
    // state information for A or B.
    // Also add any necessary methods (e.g. checksum of a String)
    Packet aPktSending = new Packet(0, 0, 0, "DEADBEEF");
    int bSeqExpecting = 0;
    boolean sending = false;
    double TIMEOUT_TIME = 20;
    double timeStart = 0;
    ArrayList<Double> rtts = new ArrayList<Double>();
    final String dummyPayload = "DEADBEEF";

    // Variables used in statistic reporting at the end of the 
    int numPktsAAppReqSent = 0;
        /* number of packets the application layer sent 
         * number of times aOutput was called */
    int numPktsAAppSent = 0;
        /* number of packets the application layer received
         * number of times aOutput called toLayer5() */
    int numPktsBAppRcvd = 0;
        /* number of packets the application layer received
         * number of times aOutput called toLayer5() */
    int numPktsATransSent = 0;
    	/* number of packets A's transport layer sent */
    int numPktsATransRcvd = 0;
    	/* number of packets A's transport layer received */
    int numPktsBTransSent = 0;
    	/* number of packets B's transport layer sent */
    int numPktsBTransRcvd = 0;
    	/* number of packets B's transport layer received */
    int numCorruptedPktsARcvd = 0;
        /* number of corrupted packets A received */
    int numCorruptedPktsBRcvd = 0;
        /* number of corrupted packets B received */
    int numDupPktsBRcvd = 0;
    	/* number of duplicate packets B received */
    int numDupPktsARcvd = 0;
    	/* number of duplicate packets A received */
    int numPktsAResent = 0;
        /* number of packets A resent 
         * when A gets a timer interrupt, 
         * aka aTimerInterrupt gets called, 
         * the current packet in transit is resent */
    int numPktsBRcvdWInvalidSeq = 0;
    	/* number of packets B received with an invalid seq num
    	 * this is also the number of packets that got lost going from B to A */
    int numAcksBSent = 0;
    	/* number of ack packets B sent */
    int numAcksAReceived = 0;
    	/* number of ack packets A received */

    /**
     * Calculates the checksum of the of the packet 
     * that would be composed with the given parameters
     * @param seq seq num
     * @param ack ack num
     * @param payload payload
     * @return the checksum
     */
    private int calcChecksum(int seq, int ack, String payload) {
        return payload.hashCode() + 3*seq + 7*ack;
    }

    /**
     * Calculates the checksum of the given packet
     * @param pkt the packet to calculate the checksum of
     * @return the checksum
     */
    private int calcChecksum(Packet pkt) {
        return (pkt.getPayload().hashCode() + 3*pkt.getSeqnum() + 7*pkt.getAcknum());
    }

    /**
     * Converts a 0 to a 1 and vice versa
     * @param bit the bit to flip
     * @return the flipped bit
     */
    private int bitFlip(int bit) {
        return (bit + 1) % 2;
    }

    // This is the constructor.  Don't touch!
    public StudentNetworkSimulator(int numMessages,
                                   double loss,
                                   double corrupt,
                                   double avgDelay,
                                   int trace,
                                   long seed)
    {
        super(numMessages, loss, corrupt, avgDelay, trace, seed);
    }

    // This routine will be called whenever the upper layer at the sender [A]
    // has a message to send.  It is the job of your protocol to insure that
    // the data in such a message is delivered in-order, and correctly, to
    // the receiving upper layer.
    protected void aOutput(Message message)
    {
        numPktsAAppReqSent++;

        // Only send the message if we aren't already sending a message
        if (sending) {
            System.out.println("Dropping message b/c I am already sending one.");
            return;
        }

        // Set sending to true so we don't accept anymore packets
        sending = true;

        // Increase the count of the packets sent by the application
        numPktsAAppSent++;

        // Create the packet
        // The seq # gets incremented when the packet is received and verified
        aPktSending.setPayload(message.getData());
        aPktSending.setChecksum(calcChecksum(aPktSending));

        // Print out info
        System.out.print("A snd pkt:  ");
        printPkt(aPktSending);
        numPktsATransSent++;
        
        timeStart = getTime();

        // send it to layer 3
        startTimer(A, TIMEOUT_TIME);
        toLayer3(A, aPktSending);
    }
    
    // This routine will be called whenever a packet sent from the B-side 
    // (i.e. as a result of a toLayer3() being done by a B-side procedure)
    // arrives at the A-side.  "packet" is the (possibly corrupted) packet
    // sent from the B-side.
    protected void aInput(Packet pkt)
    {
        System.out.print("A rcv pkt  ");
        numPktsATransRcvd++;
        printPkt(pkt);

        // Validate the checksum
        int packetChecksum = pkt.getChecksum();
        int calculatedChecksum = calcChecksum(pkt.getSeqnum(), pkt.getAcknum(), pkt.getPayload());
        // If the checksum is invalid, do nothing
        if(packetChecksum != calculatedChecksum){
               System.out.println("  Dropping corrupted packet because checksum is invalid");
               numCorruptedPktsARcvd++;
               return;
        }

        // If the packet isn't corrupted and it is coming from the B side,
        // it is an ack
        numAcksAReceived++;

        // If the ack doesn't match the current seq num do nothing
        if(pkt.getAcknum() != aPktSending.getSeqnum()){
            System.out.println("  Dropping pkt b/c ack didn't match the expected seq");
            numDupPktsARcvd++;
            return;
        }

        stopTimer(A);

        // alternate the bit of A's seq
        aPktSending.setSeqnum(bitFlip(aPktSending.getSeqnum()));

        numPktsBAppRcvd++;
        
        double rtt = getTime() - timeStart;
        rtts.add(new Double(rtt));
        
        
        sending = false;
    }


    /**
     * Prints out the given packet.  It is assumed that a 
     * previous line is printed that tells where the packet is 
     * going.  For example, A rcv pkt:  
     * @param pkt the packet to print
     */
    protected void printPkt(Packet pkt)
    {
        System.out.println("  seq: " + pkt.getSeqnum() + 
                           " ack: " + pkt.getAcknum() +
                           " checksum: " + pkt.getChecksum() + 
                           " payload: " + pkt.getPayload());
    }

    /**
     * Prints statistics of this simulation
     * Descriptions of the variables used below are given 
     * where they are declared
     */
    protected void printStatistics()
    {
        System.out.println();
        System.out.println("Statistics:  ");
        System.out.println("  # packets application tried to send:  " + numPktsAAppReqSent);
        int numPktsDroppedByApp = numPktsAAppReqSent - numPktsAAppSent;
            /* number of packets the application tried to send
             * while we were waiting to receive */
        System.out.println("  # packets A's application dropped:  " + numPktsDroppedByApp);
        System.out.println("  # packets A's application layer sent: " + numPktsAAppSent);
        System.out.println("  # packets A's transport layer sent: " + numPktsATransSent);
        System.out.println("  # packets A's transport layer received:  " + numPktsATransRcvd);
        System.out.println("  # packets A resent: " + numPktsAResent);
        System.out.println("  # packets B's application layer received: " + numPktsBAppRcvd);
        System.out.println("  # packets B's transport layer sent: " + numPktsBTransSent);
        System.out.println("  # packets B's transport layer received:  " + numPktsBTransRcvd);
        int numPktsCorrupted = numCorruptedPktsARcvd + numCorruptedPktsBRcvd;
        System.out.println("  # packets corrupted:  " + numPktsCorrupted);
        int totalPktsTransRcvd = numPktsATransRcvd + numPktsBTransRcvd;
        int percentPktsCorrupted = (int)(100 * (numPktsCorrupted / (double)(totalPktsTransRcvd)));
        System.out.println("  % packets corrupted:  " + percentPktsCorrupted + "%");
        System.out.println("  # packets corrupted received by A:  " + numCorruptedPktsARcvd);
        int percentCorruptedPktsARcvd = (int)(100 * (numCorruptedPktsARcvd / (double)numPktsATransRcvd));
        System.out.println("  % packets corrupted received by A:  " + percentCorruptedPktsARcvd + "%");
        System.out.println("  # packets corrupted received by B:  " + numCorruptedPktsBRcvd);
        int percentCorruptedPktsBRcvd = (int)(100 * (numCorruptedPktsBRcvd / (double)numPktsBTransRcvd));
        	/* tells what percent of the packets that B received were corrupted */
        System.out.println("  % packets corrupted received by B:  " + percentCorruptedPktsBRcvd + "%");
        int numPktsLostBtoA = numDupPktsBRcvd - numCorruptedPktsARcvd;
        	/* number of packets that were lost in transit from B to A */
        int numPktsLostAtoB = numPktsAResent - (numCorruptedPktsARcvd + numCorruptedPktsBRcvd + numPktsLostBtoA);
        	/* number of packets that were lost in transit from A to B */
        int numPktsLost = numPktsLostBtoA + numPktsLostAtoB;
        	/* the total number of packets that were lost in transit */
        System.out.println("  # packets lost:  " + numPktsLost);
        int percentPktsLost = (int)(100 * (numPktsLost / (double)(numPktsATransSent + numPktsBTransSent)));
        System.out.println("  % packets lost:  " + percentPktsLost + "%");
        System.out.println("  # packets lost from B to A:  " + numPktsLostBtoA);
        int percentPktsLostBtoA = (int)(100 * (numDupPktsBRcvd / (double)numPktsBTransSent));
        	/* the percentage of packets that were lost in transit from B to A */
        System.out.println("  % packets lost from B to A:  " + percentPktsLostBtoA + "%");
        System.out.println("  # packets lost from A to B:  " + numPktsLostAtoB);
        int percentPktsLostAtoB = (int)(100 * (numDupPktsARcvd / (double)numPktsBTransSent));
        	/* the percentage of packets that were lost in transit from A to B */
        System.out.println("  % packets lost from A to B:  " + percentPktsLostAtoB + "%");
        System.out.println("  # ACKs B sent:  " + numAcksBSent);
        System.out.println("  # ACKs A received:  " + numAcksAReceived);
        
        double totalRTT = 0;
        for (int i = 0; i < rtts.size(); i++) {
        	totalRTT += rtts.get(i);
        }
        double avgRTT = totalRTT / rtts.size();
        System.out.println("  avg rtt:  " + avgRTT);
    }

    // This routine will be called when A's timer expires (thus generating a 
    // timer interrupt). You'll probably want to use this routine to control 
    // the retransmission of packets. See startTimer() and stopTimer(), above,
    // for how the timer is started and stopped. 
    protected void aTimerInterrupt()
    {
        // since we stop the timer on every ack, this should only be called
        // when we need to resend something
        System.out.print("A timed out.  Resending:  ");
        numPktsATransSent++;
        numPktsAResent++;
        printPkt(aPktSending);
        startTimer(A, TIMEOUT_TIME);
        toLayer3(A, aPktSending);
    }
    
    // This routine will be called once, before any of your other A-side 
    // routines are called. It can be used to do any required
    // initialization (e.g. of member variables you add to control the state
    // of entity A).
    protected void aInit()
    {
    }
    
    // This routine will be called whenever a packet sent from the A-side 
    // (i.e. as a result of a toLayer3() being done by an A-side procedure)
    // arrives at the B-side.  "packet" is the (possibly corrupted) packet
    // sent from the A-side.
    protected void bInput(Packet pkt)
    {
    	numPktsBTransRcvd++;
        System.out.print("B rcv pkt:  ");
        printPkt(pkt);
        int packetChecksum = pkt.getChecksum();
        int calculatedChecksum = calcChecksum(pkt);

        // If the checksum is invalid, do nothing
        if(packetChecksum != calculatedChecksum){
               System.out.println("  Packet is corrupted, sending dup ack");
               numCorruptedPktsBRcvd++;
               int ack = bitFlip(bSeqExpecting);
               bSendAck(ack);
               return;
        }
        System.out.println("  Checksum is valid.");

        // Verify the seq #'s alternate
        System.out.println("Expecting seq " + bSeqExpecting);
        System.out.println("got seq " + pkt.getSeqnum());
        if(pkt.getSeqnum() != bSeqExpecting){
            System.out.println("  Got unexpected seq #, sending dup ack");
            numDupPktsBRcvd++;
            int ack = bitFlip(bSeqExpecting);
            bSendAck(ack);
            return;
        }

        int ack = bSeqExpecting;
        bSendAck(ack);
        bSeqExpecting = bitFlip(bSeqExpecting);
    }

    /**
     * Sends a packet to A with the given ack
     * @param ack the ack of the packet
     */
    private void bSendAck(int ack) {
        int seq = 0;
        	/* B's seq num is always 0 */
        int checksum = calcChecksum(seq, ack, dummyPayload);
        Packet p = new Packet(seq, ack, checksum, dummyPayload);
        System.out.print("B snd ack  ");
        printPkt(p);
        
        /* update statistics */
        numAcksBSent++;
        numPktsBTransSent++;

        toLayer3(B, p);
        return;
    }

    
    // This routine will be called once, before any of your other B-side 
    // routines are called. It can be used to do any required
    // initialization (e.g. of member variables you add to control the state
    // of entity B).
    protected void bInit()
    {
    }
}
