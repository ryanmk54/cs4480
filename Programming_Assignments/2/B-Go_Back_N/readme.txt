# Readme

## How to Compile
To compile, run:  ant

## How to Run
To run, run:  ./run.sh
If that doesn't work, run:  java Project

### Values to use when running
Without any loss or corruption, the values can be set as follows:
- number of message to simulate:  1000
- average time between message from sender's layer 5:  50
