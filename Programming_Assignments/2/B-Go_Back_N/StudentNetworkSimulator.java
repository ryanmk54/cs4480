import java.util.ArrayList;

public class StudentNetworkSimulator extends NetworkSimulator
{
    /*
     * Predefined Constants (static member variables):
     *
     *   int MAXDATASIZE : the maximum size of the Message data and
     *                     Packet payload
     *
     *   int A           : a predefined integer that represents entity A
     *   int B           : a predefined integer that represents entity B
     *
     *
     * Predefined Member Methods:
     *
     *  void stopTimer(int entity): 
     *       Stops the timer running at "entity" [A or B]
     *  void startTimer(int entity, double increment): 
     *       Starts a timer running at "entity" [A or B], which will expire in
     *       "increment" time units, causing the interrupt handler to be
     *       called.  You should only call this with A.
     *  void toLayer3(int callingEntity, Packet p)
     *       Puts the packet "p" into the network from "callingEntity" [A or B]
     *  void toLayer5(int entity, String dataSent)
     *       Passes "dataSent" up to layer 5 from "entity" [A or B]
     *  double getTime()
     *       Returns the current time in the simulator.  Might be useful for
     *       debugging.
     *  void printEventList()
     *       Prints the current event list to stdout.  Might be useful for
     *       debugging, but probably not.
     *
     *
     *  Predefined Classes:
     *
     *  Message: Used to encapsulate a message coming from layer 5
     *    Constructor:
     *      Message(String inputData): 
     *          creates a new Message containing "inputData"
     *    Methods:
     *      boolean setData(String inputData):
     *          sets an existing Message's data to "inputData"
     *          returns true on success, false otherwise
     *      String getData():
     *          returns the data contained in the message
     *  Packet: Used to encapsulate a packet
     *    Constructors:
     *      Packet (Packet p):
     *          creates a new Packet that is a copy of "p"
     *      Packet (int seq, int ack, int check, String newPayload)
     *          creates a new Packet with a sequence field of "seq", an
     *          ack field of "ack", a checksum field of "check", and a
     *          payload of "newPayload"
     *      Packet (int seq, int ack, int check)
     *          chreate a new Packet with a sequence field of "seq", an
     *          ack field of "ack", a checksum field of "check", and
     *          an empty payload
     *    Methods:
     *      boolean setSeqnum(int n)
     *          sets the Packet's sequence field to "n"
     *          returns true on success, false otherwise
     *      boolean setAcknum(int n)
     *          sets the Packet's ack field to "n"
     *          returns true on success, false otherwise
     *      boolean setChecksum(int n)
     *          sets the Packet's checksum to "n"
     *          returns true on success, false otherwise
     *      boolean setPayload(String newPayload)
     *          sets the Packet's payload to "newPayload"
     *          returns true on success, false otherwise
     *      int getSeqnum()
     *          returns the contents of the Packet's sequence field
     *      int getAcknum()
     *          returns the contents of the Packet's ack field
     *      int getChecksum()
     *          returns the checksum of the Packet
     *      int getPayload()
     *          returns the Packet's payload
     *
     */


    // Add any necessary class variables here.  Remember, you cannot use
    // these variables to send messages error free!  They can only hold
    // state information for A or B.
    // Also add any necessary methods (e.g. checksum of a String)
	
	/*
	You should hand in output for a run that was long enough so that at least 20 messages were
	successfully transferred from sender to receiver (i.e., the sender received ACKs for these messages) transfers,
	a loss probability of 0.05, and a corruption probability of 0.05, and a trace level of 2, and a mean time
	between arrivals of 10. You should annotate your output showing how your protocol correctly recovered
	from packet loss and corruption.
	
	Your should also show the statistics for your program under various loss and corruption
	probabilities. This should include: number of packets sent/received (by the application), number of packets
	sent/received by your transport protocol, number of lost packets, number of corrupted packets, number of
	ACKs sent/received etc. You should argue/show whether or not the loss and corruption values for each run
	(it should be long enough, say, with 1000 messages) are reasonable
	*/
	
    // Variables used in statistic reporting at the end of the program
	int numPktsAAppReqSent = 0;
	    /* number of packets the application layer sent 
	     * number of times aOutput was called */
	int numPktsAAppSent = 0;
	    /* number of packets the application layer received
	     * number of times aOutput called toLayer5() */
	int numPktsATransSent = 0;
		/* number of packets A's transport layer sent */
	int numPktsATransRcvd = 0;
		/* number of packets A's transport layer received */
	int numPktsBTransSent = 0;
		/* number of packets B's transport layer sent */
	int numPktsBTransRcvd = 0;
		/* number of packets B's transport layer received */
	int numPktsAResent = 0;
	    /* number of packets A resent 
	     * when A gets a timer interrupt, 
	     * aka aTimerInterrupt gets called, 
	     * the current packets in transit is resent */
	int numStreamsResent = 0;
	int numPktsBAppRcvd = 0;
	    /* number of packets the application layer received
	     * number of times aOutput called toLayer5() */
	int numCorruptedPktsARcvd = 0;
	int numCorruptedPktsBRcvd = 0;
	int numAcksARcvd = 0;
		/* number of ack packets A received */
	int numAcksBSent = 0;
		/* number of ack packets B sent */
	int numPktsBRcvdOutOfWin = 0;
	int numDupStreamsBRcvd = 0;
	boolean pktOutOfSeqAToB = false;
	int numPktsARcvdOutOfWin = 0;
	ArrayList<Double> rtts = new ArrayList<Double>();
	final String dummyPayload = "DEADBEEF";
    double TIMEOUT_TIME = 25;
    double timeStart = 0;
    Packet aPktSending = new Packet(0, 0, 0, "DEADBEEF");
    CircularBuffer circBuf = new CircularBuffer();
    final int aDummyAck = 0;
    int bSeqExpecting = 0;


    /**
     * Calculates the checksum of the of the packet 
     * that would be composed with the given parameters
     * @param seq seq num
     * @param ack ack num
     * @param payload payload
     * @return the checksum
     */
    private int calcChecksum(int seq, int ack, String payload) {
        return payload.hashCode() + 3*seq + 7*ack;
    }

    /**
     * Calculates the checksum of the given packet
     * @param pkt the packet to calculate the checksum of
     * @return the checksum
     */
    private int calcChecksum(Packet pkt) {
        return (pkt.getPayload().hashCode() + 3*pkt.getSeqnum() + 7*pkt.getAcknum());
    }

    /**
     * Sends all messages inside the window that haven't been sent
     */
    private int sendBufferedMessages(){
    	int numPktsSent = 0;
        while (circBuf.isInWindow(circBuf.firstMsgNotSent) && circBuf.firstMsgNotSent != circBuf.freePos ) {
            aSendPkt(circBuf.backingBuf[circBuf.firstMsgNotSent], circBuf.firstMsgNotSent);
            circBuf.firstMsgNotSent = circBuf.incrementPosInWindow(circBuf.firstMsgNotSent, 1, circBuf.backingBuf.length);
            numPktsSent++;
        }
        return numPktsSent;
    }

    /**
     * Sends a packet
     * @param payload the payload of the packet
     * @param seqNum the seq number of the packet
     */
    private void aSendPkt(String payload, int seqNum){
        // Create the packet
        int checksum = calcChecksum(seqNum, aDummyAck, payload);
        aPktSending.setSeqnum(seqNum);
        aPktSending.setChecksum(checksum);
        aPktSending.setPayload(payload);

        // Print out info
        System.out.print("A snd:  ");
        printPkt(aPktSending);
        numPktsATransSent++;

        // send it to layer 3
        toLayer3(A, aPktSending);
    }

    // This is the constructor.  Don't touch!
    public StudentNetworkSimulator(int numMessages,
                                   double loss,
                                   double corrupt,
                                   double avgDelay,
                                   int trace,
                                   long seed)
    {
        super(numMessages, loss, corrupt, avgDelay, trace, seed);
    }

    // This routine will be called whenever the upper layer at the sender [A]
    // has a message to send.  It is the job of your protocol to insure that
    // the data in such a message is delivered in-order, and correctly, to
    // the receiving upper layer.
    protected void aOutput(Message message)
    {
        numPktsAAppReqSent++;

        // If the buffer is full, drop the message
        if (circBuf.isFull()) {
        	System.out.println("Buffer Overflow:  Exiting!!!");
        	printStatistics();
        	System.exit(1);
        	return;
        }
        numPktsAAppSent++;
        // Increase the count of the packets sent by the application

        // Start the timer if this message is 
        // the first one in the window
        if(circBuf.windowBase == circBuf.firstMsgNotSent){
            startTimer(A, TIMEOUT_TIME);
            timeStart = getTime();
        }

        // Add the message to queue and send all buffered messages
        circBuf.addMessageToQueue(message.getData());
        sendBufferedMessages();
    }
    
    // This routine will be called whenever a packet sent from the B-side 
    // (i.e. as a result of a toLayer3() being done by a B-side procedure)
    // arrives at the A-side.  "packet" is the (possibly corrupted) packet
    // sent from the B-side.
    protected void aInput(Packet packet)
    {
        numPktsATransRcvd++;
        System.out.print("A rcv:  ");
        printPkt(packet);

        // Validate the checksum
        int packetChecksum = packet.getChecksum();
        int calculatedChecksum = calcChecksum(packet);

        // If the checksum is invalid, do nothing
        if(packetChecksum != calculatedChecksum){
               System.out.println("  Dropping corrupted packet " + 
                                  "because checksum is invalid");
               numCorruptedPktsARcvd++;
               return;
        }

        // If the packet isn't corrupted and it is coming from the B side,
        // it is an ack
        numAcksARcvd++;

        // commented out b/c the FSM doesn't handle this case and 
        // an ack outside of the window is never sent
        // If the ack isn't within the window, do nothing
        int ack = packet.getAcknum();
        //if (!circBuf.isInWindow(ack)) {
        //	numPktsARcvdOutOfWin++;
        //    System.out.println("  Dropping pkt b/c ack was outside of the window");
        //    return;
        //}

        // update the number of packets received by the application
        numPktsBAppRcvd += circBuf.forwardDistBetweenNums(circBuf.windowBase, 
              ack, circBuf.backingBuf.length);

        // move the window base to the position after the ack
        circBuf.windowBase = circBuf.incrementPosInWindow(ack, 1, circBuf.backingBuf.length);
        //if (circBuf.forwardDistBetweenNums(circBuf.firstMsgNotSent, circBuf.windowBase, circBuf.backingBuf.length) > 8) {
        //    System.out.println(" dist between nums:  " + circBuf.forwardDistBetweenNums(circBuf.firstMsgNotSent, circBuf.windowBase, circBuf.backingBuf.length));
        //    System.out.println("  firstMsgNotSent:  " + circBuf.firstMsgNotSent);
        //    System.out.println("  windowBase:  " + circBuf.windowBase);
        //    circBuf.firstMsgNotSent = circBuf.windowBase;
        //}
        System.out.println("Received an ack inside the window.  Resetting the timer.");
        stopTimer(A);

        // Update the rtt
        double rtt = getTime() - timeStart;
        rtts.add(new Double(rtt));

        // Only start the timer if we have messages left in the queue
        if(circBuf.windowBase != circBuf.freePos){
            startTimer(A, TIMEOUT_TIME);
        }
    }


    protected void printPkt(Packet pkt)
    {
        System.out.println("seq: " + pkt.getSeqnum() + 
                           " ack: " + pkt.getAcknum() +
                           " checksum: " + pkt.getChecksum() + 
                           " payload: " + pkt.getPayload());
    }

    protected void printStatistics()
    {
    	 System.out.println();
         System.out.println("Statistics:  ");
         System.out.println("  # packets A's application layer sent: " + numPktsAAppSent);
         System.out.println("  # packets A's transport layer sent: " + numPktsATransSent);
         System.out.println("  # packets A's transport layer received:  " + numPktsATransRcvd);
         System.out.println("  # packets A resent: " + numPktsAResent);
         System.out.println("  # packets B's application layer received: " + numPktsBAppRcvd);
         System.out.println("  # packets B's transport layer sent: " + numPktsBTransSent);
         System.out.println("  # packets B's transport layer received:  " + numPktsBTransRcvd);
         int numPktsCorrupted = numCorruptedPktsARcvd + numCorruptedPktsBRcvd;
         System.out.println("  # corrupted packets received:  " + numPktsCorrupted);
         int totalPktsTransRcvd = numPktsATransRcvd + numPktsBTransRcvd;
         int percentPktsCorrupted = (int)(100 * (numPktsCorrupted / (double)(totalPktsTransRcvd)));
         System.out.println("  % packets corrupted:  " + percentPktsCorrupted + "%");
         System.out.println("  # corrupted packets A received:  " + numCorruptedPktsARcvd);
         int percentCorruptedPktsARcvd = (int)(100 * (numCorruptedPktsARcvd / (double)numPktsATransRcvd));
         System.out.println("  % packets A received that were corrupted:  " + percentCorruptedPktsARcvd + "%");
         System.out.println("  # packets B received that were corrupted:  " + numCorruptedPktsBRcvd);
         int percentCorruptedPktsBRcvd = (int)(100 * (numCorruptedPktsBRcvd / (double)numPktsBTransRcvd));
         System.out.println("  % packets B received that were corrupted:  " + percentCorruptedPktsBRcvd + "%");

         int numPktsInTransit = circBuf.forwardDistBetweenNums(circBuf.windowBase, circBuf.freePos, circBuf.backingBuf.length);
         System.out.println("  # packets probably in transit  " + numPktsInTransit);
         int numPktsLostAtoB = numPktsATransSent - numPktsBTransRcvd;
         int numPktsLostBtoA = numPktsBTransSent - numPktsATransRcvd;

         int numPktsLost = numPktsLostBtoA + numPktsLostAtoB;
         	/* the total number of packets that were lost in transit */
         System.out.println("  # packets lost:  " + numPktsLost);
         int percentPktsLost = (int)(100 * (numPktsLost / (double)(numPktsATransSent + numPktsBTransSent)));
         System.out.println("  % packets lost:  " + percentPktsLost + "%");
         System.out.println("  # packets lost from B to A:  " + numPktsLostBtoA);
         int percentPktsLostBtoA = (int)(100 * (numPktsLostBtoA / (double)numPktsBTransSent));
         	/* the percentage of packets that were lost in transit from B to A */
         System.out.println("  % packets lost from B to A:  " + percentPktsLostBtoA + "%");
         System.out.println("  # packets lost from A to B:  " + numPktsLostAtoB);
         int percentPktsLostAtoB = (int)(100 * (numPktsLostAtoB / (double)numPktsATransSent));
         	/* the percentage of packets that were lost in transit from A to B */
         System.out.println("  % packets lost from A to B:  " + percentPktsLostAtoB + "%");
         System.out.println("  # ACKs B sent:  " + numAcksBSent);
         System.out.println("  # ACKs A received:  " + numAcksARcvd);
         
         double totalRTT = 0;
         for (int i = 0; i < rtts.size(); i++) {
         	totalRTT += rtts.get(i);
         }
         double avgRTT = totalRTT / rtts.size();
         System.out.println("  avg rtt:  " + avgRTT);
    }

    // This routine will be called when A's timer expires (thus generating a 
    // timer interrupt). You'll probably want to use this routine to control 
    // the retransmission of packets. See startTimer() and stopTimer(), above,
    // for how the timer is started and stopped. 
    protected void aTimerInterrupt()
    {
        System.out.println("A timed out.  Resending:  ");
        
        circBuf.firstMsgNotSent = circBuf.windowBase;
        	// Resend all packets in the window
        startTimer(A, TIMEOUT_TIME);
        	// Restart the timer
        numPktsAResent += sendBufferedMessages();
        numStreamsResent++;
    }
    
    // This routine will be called once, before any of your other A-side 
    // routines are called. It can be used to do any required
    // initialization (e.g. of member variables you add to control the state
    // of entity A).
    protected void aInit()
    {
    }
    
    // This routine will be called whenever a packet sent from the A-side 
    // (i.e. as a result of a toLayer3() being done by an A-side procedure)
    // arrives at the B-side.  "packet" is the (possibly corrupted) packet
    // sent from the A-side.
    protected void bInput(Packet packet)
    {
    	numPktsBTransRcvd++;
        System.out.print("B rcv:  ");
        printPkt(packet);
        
        // If the checksum is invalid, do nothing
        int packetChecksum = packet.getChecksum();
        int calculatedChecksum = calcChecksum(packet);
        if(packetChecksum != calculatedChecksum){
               System.out.println("  Dropping corrupted packet");
               numCorruptedPktsBRcvd++;
               return;
        }
        System.out.print("  Checksum is valid -- ");

        // Verify the seq #
        System.out.print("Expecting seq " + bSeqExpecting);
        System.out.println(" got seq " + packet.getSeqnum());
        if(packet.getSeqnum() != bSeqExpecting){
            System.out.println("  Received invalid packet.  Sending dup ack");
            numPktsBRcvdOutOfWin++;
            
            if (!pktOutOfSeqAToB) {
            	numDupStreamsBRcvd++;
            }
            pktOutOfSeqAToB = true;
            int seq = 0;
            int ack = bSeqExpecting - 1;
            if (ack == -1) {
                ack = 49;
            }
            String payload = "DEADBEEF";
            int checksum = calcChecksum(seq, ack, payload);
            Packet p = new Packet(seq, ack, checksum, payload);
            printPkt(p);
            numAcksBSent++;
            numPktsBTransSent++;
            toLayer3(B, p);
            return;
        }
        pktOutOfSeqAToB = false;
        
        bSeqExpecting = circBuf.incrementPosInWindow(bSeqExpecting, 1, circBuf.backingBuf.length);

        // Send back an ack
        int seq = 0;
        int ack = packet.getSeqnum();
        String payload = "DEADBEEF";
        int checksum = calcChecksum(seq, ack, payload);
        Packet p = new Packet(seq, ack, checksum, payload);
        System.out.print("B snd:  ");
        printPkt(p);
        numAcksBSent++;
        numPktsBTransSent++;
        toLayer3(B, p);
    }
    
    // This routine will be called once, before any of your other B-side 
    // routines are called. It can be used to do any required
    // initialization (e.g. of member variables you add to control the state
    // of entity B).
    protected void bInit()
    {
    }
}
