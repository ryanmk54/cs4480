
public class CircularBuffer {
	final int windowSize = 8;
	public String[] backingBuf = new String[50];
		/* the buffer that holds the messages 
		 * we are sending and the messages queued to send */
	public int windowBase = 0;
		// the position of the first message not yet ack'd
	public int freePos = 0;
		// backingBuf should be free from freePosition to windowBase
	public int firstMsgNotSent = 0;
		// the first message that has not yet been sent
	
	public CircularBuffer() {
		
	}
	
	public boolean isFull() {
		return freePos == windowBase - 1;
	}
	
	public boolean hasRoom(){
		return freePos != windowBase - 1;
	}

	public int roomLeft() {
		return backingBuf.length - 1 - forwardDistBetweenNums(windowBase, freePos, backingBuf.length);
	}
	
	public boolean isInWindow(int pos) {
		return isPosInWindow(pos, windowBase, windowSize, backingBuf.length);
	}

	public boolean isEmpty() {
		return windowBase == freePos;
	}
	
	/**
	 * Adds a message to the queue and moves
	 * the free pos forward one to account for this
	 * @param payload the message to queue
	 */
	public void addMessageToQueue(String payload) {
		backingBuf[freePos] = payload;
		freePos = incrementPosInWindow(freePos, 1, backingBuf.length);
	}

    public int incrementPosInWindow(int pos, int incrementBy, int windowSize){
        return (pos + incrementBy) % windowSize;
    }
    
    public boolean isPosInWindow(int pos, int base, int windowSize, int bufferSize){
        if(base + windowSize <= bufferSize){
            return base <= pos && pos < base + windowSize;
        }
        else{
            return base <= pos && pos < base + windowSize ||
                   0 < pos && pos < incrementPosInWindow(base, 
                                                         windowSize, 
                                                         bufferSize);
        }
    }
    
    public int forwardDistBetweenNums(int before, int after, int bufSize){
        if(after < before){
            return bufSize + after - before;
        } 
        else{
            return after - before + 1;
        }
    }
}
