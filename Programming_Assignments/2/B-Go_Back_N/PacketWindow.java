/**
 * This class is not thread safe
 */
public class PacketWindow{
    /* If base == null, the array is empty
     * If nextFreePosition == base, the array is full
     * If nextFreePosition != null, the array is not full
     */

    public String[] backingArray;
        // Holds the payloads of the messages being sent
        // Null means the position is free
    public int base;
        // next position to free
    public int nextFreePosition;
        // next position to fill

    public PacketWindow(){
        this(50);
    }

    public PacketWindow(int length){
        backingArray = new String[length];
        base = 0;
        nextFreePosition = 0;
    }

    public int getBase(){
        return base;
    }

    public boolean full(){
        return base == nextFreePosition;
    }

    public boolean empty(){
        return backingArray[base] == null;
    }

    public boolean hasRoom(){
        return backingArray[nextFreePosition] == null;
    }

    public boolean hasMessagesToSend(){
        return backingArray[base] != null;
    }

    // Empties all spots upto and including the given position
    public void emptyToPosition(int position){
        if(position > backingArray.length || position < 0){
            throw new ArrayIndexOutOfBoundsException(position);
        }

        // TODO I need some way to make sure the base doesn't pass up the nextFreePosition
        while(base != position){
            backingArray[base] = null;
            incrementHead();
        }
    }

    /**
     * @return true on success, false if the array is full
     */
    public boolean add(String payload){
        if(nextFreePosition == base){
            return false;
        }
        backingArray[nextFreePosition] = payload;
        incrementTail();
        return true;
    }

    private void incrementHead(){
        if(base == backingArray.length - 1){
            base = 0;
        }
        else{
            base++;
        }
    }

    private void incrementTail(){
        if(nextFreePosition == backingArray.length - 1){
            nextFreePosition = 0;
        }
        else{
            nextFreePosition++;
        }
    }
}
