import sys

HELLO_MESSAGE = "Hello Bob"

# Prints a message only if verbose is true
def log(verbose, output):
  if verbose:
    print output

# Signal handler to exit gracefully when Ctrl+C is pressed
def signal_handler(signal, frame):
  print ""
  print "Exiting"
  sys.exit(0)

