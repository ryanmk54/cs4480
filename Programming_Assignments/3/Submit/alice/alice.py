#! /usr/bin/env python

from Crypto import Random
from Crypto.Cipher import DES3
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Padding import *
from shared import *

import argparse
import base64
import binascii
import json
import signal
import socket
import sys

ALICE_PRIV_KEY_PATH = "alice-keys/alice-priv-key.pem"
ALICE_PUB_KEY_PATH = "alice-keys/alice-pub-key.pem"
BOB_CA_PUB_KEY_PATH = "alice-keys/bob-ca-pub-key.pem"

def hash_msg(msg):
  hash = SHA.new(msg)
  log(args.verbose, "Message Hash (hex):")
  log(args.verbose, hash.hexdigest())
  log(args.verbose, "")
  return hash.digest()

def sign_hash(key, hash):
  """
  Args:
    key:  RSA key
    hash: SHA hash
  Returns:
    The signature of the hash (a long)
  """
  signature = key.sign(hash, None)[0]
  log(args.verbose, "Hash signature:")
  log(args.verbose, signature)
  log(args.verbose, "")
  return signature

def verify_hash(pub_key, msg_hash, sig):
  sig_tuple = (sig, None)
  status =  pub_key.verify(msg_hash, sig_tuple)
  if status:
      log(args.verbose, "Message hash verified")
  else:
      log(args.verbose, "Unable to verify hash")
  log(args.verbose, "")
  return status

def encrypt_sym_key_w_pub_key(pub_key, sym_key):
  """
    Returns: hexlified encrypted symmetric key
  """
  encrypted_sym_key = pub_key.encrypt(sym_key, None)[0]
  hexlified_enc_sym_key = binascii.hexlify(encrypted_sym_key);
  log(args.verbose, "Encrypted Symmetric Key:")
  log(args.verbose, hexlified_enc_sym_key)
  log(args.verbose, "")
  return hexlified_enc_sym_key

def encrypt_w_sym(symmetric_key, msg_to_enc):
  padded_obj = appendPadding(msg_to_enc, blocksize=DES_blocksize)
  symmetric_cipher = DES3.new(symmetric_key)
  encrypted_obj = symmetric_cipher.encrypt(padded_obj)
  hexlified_encrypted_obj = binascii.hexlify(encrypted_obj)
  log(args.verbose, "Symmetric Encrypted Message:")
  log(args.verbose, hexlified_encrypted_obj)
  log(args.verbose, "")
  return hexlified_encrypted_obj

# Register sigint to exit gracefully
signal.signal(signal.SIGINT, signal_handler)

parser = argparse.ArgumentParser() 
parser.add_argument("-v", "--verbose", action="store_true",
                    help="Turn on verbose mode")
parser.add_argument("hostname", 
                    help="The hostname Bob's secure messaging app is "
                         "running on")
parser.add_argument("port", 
                    help="The port Bob's secure messaging app is running on")
args = parser.parse_args()


log(args.verbose, "Connecting to " + args.hostname + ":" + args.port)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try :
  sock.connect((args.hostname, int(args.port)))
except socket.error as e:
  print "Unable to connect to Bob"
  print e
  exit(-1)
except socket.gaierror as e:
  print "Unable to find Bob's hostname"
  print e
  exit(-1)

alice_msg = raw_input("What secret message would you like to send to Bob?  ")

log(args.verbose, "Sending Hello message")
log(args.verbose, "")
sock.sendall(HELLO_MESSAGE)

# Receive Bob's public key
json_data = ''
# TODO what if this doesn't all fit in 1024?
data = sock.recv(1024)
json_data += data

sent_data = json.loads(json_data)
bob_ca_pub_key = RSA.importKey(open(BOB_CA_PUB_KEY_PATH, 'r').read())
bob_pub_key_pem = sent_data[0]
bob_pub_key_sha = hash_msg(bob_pub_key_pem)
bob_pub_key_sha_sig = sent_data[1]
log(args.verbose, "Received Bob's public key:")
log(args.verbose,  bob_pub_key_pem)
log(args.verbose, "")
log(args.verbose, "Received Bob's CA signature of Bob's public key:")
log(args.verbose, bob_pub_key_sha_sig)

if verify_hash(bob_ca_pub_key, bob_pub_key_sha, bob_pub_key_sha_sig):
  log(args.verbose, "Verified Bob's public key")
  log(args.verbose, "")
else :
  print "Unable to verify Bob's public key."
  print "Exiting"
  sys.exit(-1)

#Send the message
alice_priv_key = RSA.importKey(open(ALICE_PRIV_KEY_PATH, 'r').read())
#bob_pub_key = RSA.importKey(open(BOB_PUB_KEY_PATH, 'r').read())
bob_pub_key = RSA.importKey(bob_pub_key_pem)
symmetric_key = Random.new().read(16)
symmetric_cipher = DES3.new(symmetric_key)
log(args.verbose, "Alice's message is:")
log(args.verbose, alice_msg)
log(args.verbose, "")

# Signed msg hash + msg
alice_pub_key = RSA.importKey(open(ALICE_PRIV_KEY_PATH, 'r').read())
hashed_msg = hash_msg(alice_msg)
signed_msg_hash = sign_hash(alice_priv_key, hashed_msg)
json_signed_msg_hash_and_msg = json.dumps((signed_msg_hash, alice_msg))

# Encrypted msg hash + msg
encrypted_msg_hash_and_msg = encrypt_w_sym(symmetric_key, json_signed_msg_hash_and_msg)

# Encrypted symmetric key
encrypted_sym_key = encrypt_sym_key_w_pub_key(bob_pub_key, symmetric_key)

# Everything Alice is sending over
alice_json = json.dumps((encrypted_msg_hash_and_msg, encrypted_sym_key))

log(args.verbose, "Alice is sending over:")
log(args.verbose, alice_json)
sock.sendall(alice_json)

