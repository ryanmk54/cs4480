import sys

HELLO_MESSAGE = "Hello Bob"

def log(verbose, output):
  if verbose:
    print output

def signal_handler(signal, frame):
  print ""
  print "Exiting"
  sys.exit(0)

