#! /usr/bin/env python

from Crypto.Cipher import DES3
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA
from Crypto.PublicKey import RSA
from Padding import *
from shared import *

import argparse
import base64
import binascii
import json
import signal
import socket

ALICE_PUB_KEY_PATH = "bob-keys/alice-pub-key.pem"
BOB_CA_PRIV_KEY_PATH = "bob-keys/bob-ca-priv-key.pem"
BOB_CA_PUB_KEY_PATH = "bob-keys/bob-ca-pub-key.pem"
BOB_PRIV_KEY_PATH = "bob-keys/bob-priv-key.pem"
BOB_PUB_KEY_PATH = "bob-keys/bob-pub-key.pem"

def hash_msg(msg):
  hash = SHA.new(msg)
  log(args.verbose, "Message Hash (hex):")
  log(args.verbose, hash.hexdigest())
  log(args.verbose, "")
  return hash.digest()

def sign_hash(key, msg_hash):
  """
  Args:
    key:  RSA key
    hash: SHA hash digest
  Returns:
    The signature of the hash (a long)
  """
  signature = key.sign(msg_hash, None)[0]
  log(args.verbose, "Hash signature:")
  log(args.verbose, signature)
  log(args.verbose, "")
  return signature

def verify_hash(pub_key, msg_hash, rsa_sig):
  actual_sig = (rsa_sig, None)
  status =  pub_key.verify(msg_hash, actual_sig)
  if status:
      log(args.verbose, "Message hash verified")
  else:
      log(args.verbose, "Unable to verify hash")
  log(args.verbose, "")
  return status

# Register sigint to exit gracefully
signal.signal(signal.SIGINT, signal_handler)

# Parse arguments
parser = argparse.ArgumentParser() 
parser.add_argument("-v", "--verbose", action="store_true",
                    help="Turn on verbose mode")
parser.add_argument("my_hostname", 
                    help="Hostname this secure messaging app is running on")
parser.add_argument("my_port",
                    help="Port this secure messaging app is running on")
args = parser.parse_args()

# Listen for connections
listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_sock.bind((args.my_hostname, int(args.my_port)))
listen_sock.listen(0)
print "Listening on " + args.my_hostname + ":" + args.my_port
conn_sock, addr = listen_sock.accept()
log(args.verbose, "Connected to Alice on " + addr[0] + ":" + str(addr[1]))

# Listen for Hello message
log(args.verbose, "Listening for Hello message")
hello_msg = ''
while 1 :
  hello_msg += conn_sock.recv(64)
  if hello_msg == HELLO_MESSAGE :
    log(args.verbose, "Received Hello Message")
    break
  else :
    log(args.verbose, "Received strange info from Alice")
log(args.verbose, "")
listen_sock.close()

#Received Hello message, send public key signed by private CA key
bob_ca_priv_key = RSA.importKey(open(BOB_CA_PRIV_KEY_PATH, 'r').read())
bob_pub_key_pem = open(BOB_PUB_KEY_PATH, 'r').read()
bob_pub_key_sha = hash_msg(bob_pub_key_pem)
bob_pub_key_sha_sig = sign_hash(bob_ca_priv_key, bob_pub_key_sha)
data_to_send = (bob_pub_key_pem, bob_pub_key_sha_sig)
json_data = json.dumps(data_to_send)
log(args.verbose, "Sending Bob's public key:")
log(args.verbose, bob_pub_key_pem)
log(args.verbose, "Sending Bob's CA signature of Bob's public key:")
log(args.verbose, bob_pub_key_sha_sig)
conn_sock.sendall(json_data)
log(args.verbose, "Sent Bob's public key and the signature")
log(args.verbose, "")

def decrypt_sym_key_w_priv_key(priv_key, enc_sym_key):
  binary_enc_sym_key = binascii.unhexlify(enc_sym_key)
  sym_key = priv_key.decrypt(binary_enc_sym_key)
  log(args.verbose, "Symmetric Key:")
  log(args.verbose, binascii.hexlify(sym_key))
  log(args.verbose, "")
  return sym_key

def decrypt_w_sym_key(sym_key, obj_to_dec):
  binary_obj_to_dec = binascii.unhexlify(obj_to_dec)
  symmetric_cipher = DES3.new(sym_key)
  decrypted_obj = symmetric_cipher.decrypt(binary_obj_to_dec)
  unpadded_obj = removePadding(decrypted_obj, blocksize=DES_blocksize)
  log(args.verbose, "Symmetric Decrypted Message:")
  log(args.verbose, unpadded_obj)
  log(args.verbose, "")
  return unpadded_obj

alice_msg = ""
while 1 :
  data = conn_sock.recv(1024)
  if not data: break
  alice_msg += data

bob_priv_key = RSA.importKey(open(BOB_PRIV_KEY_PATH, 'r').read())
alice_json = json.loads(alice_msg)

# Decrypt the symmetric key
encrypted_sym_key = alice_json[1]
sym_key = decrypt_sym_key_w_priv_key(bob_priv_key, encrypted_sym_key)

# Decrypt the msg hash and msg
encrypted_msg_hash_and_msg = alice_json[0]
decrypted_msg_hash_and_msg = decrypt_w_sym_key(sym_key, encrypted_msg_hash_and_msg)

# Verify the message
alice_pub_key = RSA.importKey(open(ALICE_PUB_KEY_PATH, 'r').read())
json_decrypted_msg_hash_and_msg = json.loads(decrypted_msg_hash_and_msg)
msg = json_decrypted_msg_hash_and_msg[1]
log(args.verbose, "Msg:")
log(args.verbose, msg)
log(args.verbose, "")
msg_hash = hash_msg(msg)
signed_msg_hash = json_decrypted_msg_hash_and_msg[0]
if verify_hash(alice_pub_key, msg_hash, signed_msg_hash):
  print "Alice's message is:  " + msg
else:
  print "Unable to verify that Alice actually sent this message"

conn_sock.close()
