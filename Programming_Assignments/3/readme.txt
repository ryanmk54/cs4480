Open Ports:
  A discussion has been posted on the forums that says ports 2112 - 2120
  are open on the CADE machines

How to run:
  Start Bob with the command:   python bob.py -v my_hostname my_port
  where my_hostname is the hostname of the computer bob is running on
  and my_port is the port number bob should listen on

  After starting Bob, start Alice with the command:  
  python alice alice.py -v hostname port
  where hostname is the hostname of Bob's computer
  and port is the port Bob is listening on.
  
  After starting Alice, she will ask what message you want to send to Bob.
  Enter the message you want to send to Bob and press enter.
