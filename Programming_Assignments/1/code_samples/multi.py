from multiprocessing import Process, Lock
from time import sleep

def f(l, i):
    l.acquire()
    print 'start world', i
    l.release()
    sleep(10-i)
    l.acquire()
    print 'end world', i
    l.release()

    

if __name__ == '__main__':
    lock = Lock()

    for num in range(5):
        Process(target=f, args=(lock, num)).start()
