from socket import *

serverPort = 12333
serverAddress = 'localhost'

clientSocket = socket(AF_INET, SOCK_STREAM)
print 'Bound to: (after socket call)', clientSocket.getsockname()

clientSocket.connect((serverAddress,serverPort))
print 'Bound to: (after connect call)', clientSocket.getsockname()

sentence = raw_input('Input lowercase sentence:')
clientSocket.send(sentence)

modifiedSentence = clientSocket.recv(1024)
print 'From Server:', modifiedSentence

sentence = raw_input('enter to finish')

clientSocket.close()
