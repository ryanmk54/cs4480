from socket import *

serverPort = 12333
serverAddress = 'localhost'

serverSocket = socket(AF_INET,SOCK_STREAM)
print 'Got a socket with fd:', serverSocket.fileno()

serverSocket.bind((serverAddress,serverPort))
print 'Bound to:', serverSocket.getsockname()

serverSocket.listen(1)
while 1:
    print 'Listening for requests'
    connectionSocket, addr = serverSocket.accept()
    print 'Accepted connection from:', connectionSocket.getpeername(), ' fd: ' , connectionSocket.fileno()
    sentence = connectionSocket.recv(1024)
    print "Got: ", sentence
    capitalizedSentence = sentence.upper()
    print "Sending: ", capitalizedSentence
    connectionSocket.send(capitalizedSentence)
    connectionSocket.close()
    print "Done with this one"
