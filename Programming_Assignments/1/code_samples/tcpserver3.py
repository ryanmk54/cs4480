import socket
import select
import sys

serverPort = 12333
serverAddress = 'localhost'

serverSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print 'Got a socket:', serverSocket.fileno()

serverSocket.bind((serverAddress,serverPort))
print 'Bound to:', serverSocket.getsockname()

serverSocket.listen(1)

input = [serverSocket]

while 1:

    inputready,outputready,exceptready = select.select(input,[],[])

    for s in inputready:

        if s == serverSocket:
            print 'Handle server socket'
            connectionSocket, addr = serverSocket.accept()
            print 'Accepted connection from:', connectionSocket.getpeername()
            input.append(connectionSocket)

        else :
            print 'Handle client socket for: ', s.getpeername()
            sentence = s.recv(1024)
            capitalizedSentence = sentence.upper()
            s.send(capitalizedSentence)
            s.close()
            input.remove(s)

