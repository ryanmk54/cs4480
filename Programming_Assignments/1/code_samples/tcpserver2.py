import socket
import sys

serverPort = 12333
serverAddress = None


for res in socket.getaddrinfo(serverAddress, serverPort, socket.AF_UNSPEC,
                              socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
    af, socktype, proto, canonname, sa = res
    print "We got res:", res
    try:
        print "Attempt.."
        s = socket.socket(af, socktype, proto)
    except socket.error as msg:
        s = None
        continue
    try:
        print 'Got socket: ', s.fileno()
        s.bind(sa)
        print 'Bound to: ', s.getsockname()
        s.listen(1)
        print 'Listening..'
    except socket.error as msg:
        s.close()
        s = None
        continue
    break
if s is None:
    print 'could not open socket'
    sys.exit(1)

while 1:
    print 'Listening for requests'

    connectionSocket, addr = s.accept()
    print 'Accepted connection from:', connectionSocket.getpeername()

    sentence = connectionSocket.recv(1024)
    print "Got: ", sentence

    capitalizedSentence = sentence.upper()

    print "Sending: ", capitalizedSentence
    connectionSocket.send(capitalizedSentence)
    connectionSocket.close()
    print "Done with this one"
