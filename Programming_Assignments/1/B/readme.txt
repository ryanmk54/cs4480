PA 1 - B: Basic Proxy

To start the proxy, use the command python3 proxy.py <port_number>
For example, python3 proxy.py 6000

The proxy should accept requests in absolute and relative url format.  It seems to work on Firefox 15.  I was able to load google.com osnews.com and arstechnica.com

