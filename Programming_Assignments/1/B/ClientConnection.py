import threading
from socket import *
from web_utils import *

class ClientConnection(threading.Thread):
    def __init__(self, conn_to_client):
        threading.Thread.__init__(self)
     # If the client or server sends something that we can't parse, an exception
        # will be thrown, this will catch it, close the client connection so another
        # client can connect
        try:
            request_from_client = ''
            # Take in the request from the client until we get a /r/n/r/n
            try:
                while '\r\n\r\n' not in request_from_client:
                    request_as_bytes = conn_to_client.recv(1024)
                    if not request_as_bytes:
                        print("Closing conn")
                        conn_to_client.close()
                        return
                    request_from_client +=  request_as_bytes.decode('ascii')
                print("Received {}".format(request_from_client))
            # On a site I visited on firefox, 
            # some of the requests contained non-ascii data, this handles that
            except UnicodeDecodeError:
                print("Unable to decode request")
                conn_to_client.send(generate_http_error_message(400))
                conn_to_client.close()
                return

            # Extract the header info out of the header
            request_message_lines = request_from_client.splitlines()
            request_line = request_message_lines[0]
            request_line_fields = request_line.split(' ')
            http_method = request_line_fields[0]
            url = request_line_fields[1]

            if http_method != 'GET':
                # Close the connection to the client
                conn_to_client.close()
                print("Connection to client closed because there was no get method")

            url_info = extract_info_from_url(url)
            relative_url = url_info['relative_url']
            hostname = url_info['hostname']
            port = url_info['port']
            http_version = request_line_fields[2]

            # Extract the header information that we won't use
            # (except for maybe the Host) so we can pass it on
            if len(request_message_lines) > 1:
                http_headers = request_message_lines[1:]
                if is_url_relative(url):
                    valid_http_headers, hostname = validate_http_headers(
                            http_headers, True)
                else:
                    valid_http_headers = validate_http_headers(
                            http_headers, False)[0]

            # Compose the request we'll send to the server
            #valid_http_headers = []
            request_to_server = '{http_method} {url} HTTP/1.0\r\n'.format(
                     http_method=http_method, url=relative_url)
            request_to_server += 'Host: {host}\r\n'.format(host=hostname)
            request_to_server += 'Connection: close\r\n'
            request_to_server += '\r\n'.join(valid_http_headers)
            request_to_server += '\r\n\r\n'

            # Send the request to the server
            print("Sending to {hostname} on {port}:".format(
                hostname=hostname, port=port))
            print(request_to_server)
            conn_to_server = socket(AF_INET, SOCK_STREAM)
            conn_to_server.connect((hostname, port))
            conn_to_server.send(bytes(request_to_server, 'UTF-8'))
            print("Request sent")

            # Receive everything from the server and send it to the client
            complete_file = b''
            while 1:
                file_chunk = conn_to_server.recv(1024, MSG_WAITALL);
                complete_file += file_chunk
                if len(file_chunk) == 0:
                    break

            # Close the connection to the server and send it to the client
            conn_to_server.close()
            print("Connection to server closed")
            print("Sending to client:")
            #print(complete_file)
            conn_to_client.send(complete_file)
        except Exception as e:
            print("Error: {exception}".format(exception=e))

        finally:
            # Close the connection to the client
            conn_to_client.close()
            print("Connection to client closed")
