#!/usr/bin/env python3
import unittest
from url_utilities import *

class TestStringMethods(unittest.TestCase):

    def test_extract_info_from_url(self):
        google_urls = [
            'http://google.com/',
            'http://google.com']
        for url in google_urls:
            correct_google_url_info = {
                    'relative_url': '/',
                    'hostname': 'google.com',
                    'port': 80}
            url_info = extract_info_from_url(url)
            self.assertEqual(url_info, correct_google_url_info)

        root_url = '/'
        correct_root_url_info = {
                'relative_url': '/',
                'hostname': None,
                'port': 80}
        root_url_info = extract_info_from_url(root_url)
        self.assertEqual(root_url_info, correct_root_url_info)

        kobus_absolute_url = 'http://cs.utah.edu/~kobus/simple.html'
        correct_kobus_absolute_url_info = {
                    'relative_url': '/~kobus/simple.html',
                    'hostname': 'cs.utah.edu',
                    'port': 80}
        kobus_absolute_url_info = extract_info_from_url(kobus_absolute_url)
        self.assertEqual(kobus_absolute_url_info, correct_kobus_absolute_url_info)
        correct_kobus_relative_url_info = {
                    'relative_url': '/~kobus/simple.html',
                    'hostname': None,
                    'port': 80}
        kobus_relative_url = '/~kobus/simple.html'
        kobus_relative_url_info = extract_info_from_url(kobus_relative_url)
        self.assertEqual(kobus_relative_url_info, correct_kobus_relative_url_info)


if __name__ == '__main__':
    unittest.main()
