#!/usr/bin/env python3
from ClientConnection import *
import threading
import signal
import sys
from socket import *


def sigint_handler(signal, frame):
    print("# of active threads {}".format(threading.active_count()))
    sys.exit(0)


def main():
    print('Starting proxy server')
    signal.signal(signal.SIGINT, sigint_handler)
    serverPort = int(sys.argv[1])
    serverSocket = socket(AF_INET,SOCK_STREAM)
    serverSocket.bind(('0.0.0.0', serverPort))
    serverSocket.listen(1)
    print('Listening on port {}'.format(serverPort))
    while 1:
        print('Accepting connections')
        conn_to_client, addr = serverSocket.accept()
        print("Connected to {}".format(addr))

        client_handling_thread = ClientConnection(conn_to_client)
        client_handling_thread.start()

if  __name__ =='__main__': main()
