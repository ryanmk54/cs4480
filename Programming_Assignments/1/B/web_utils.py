# Generates an http response error message
# given an http status code
def generate_http_error_message(number):
    error_message = 'invalid'
    if(number == 501):
        error_message = 'Not Implemented'
    elif(number == 400):
        error_message = 'Bad Request'
    return """
        HTTP/1.0 {number} {error_message}

        <html>
        <head>
        <title>{number} {error_message}</title>
        </head>
        <body>
        <h1>{number} {error_message}</h1>
        </body>
        </html>
    """.format(number=number, error_message=error_message)


# Converts an absolute url into a relative url
def make_url_relative(url):
    if is_url_relative(url):
        return url

    pos_start_hostname = url.find('://') + len('://')
    pos_end_hostname = url.find('/', pos_start_hostname)
    if pos_end_hostname == -1:
        relative_url = '/'
    else:
        relative_url = url[pos_end_hostname:]
    return relative_url

# Determines if a url is relative
# If the url starts with a /, we say it is relative.  
# Otherwise it is absolute.
def is_url_relative(url):
    if url.startswith('/'):
            return True
    return False

# returns a key consisting of the: 
#     relative url,
#     hostname,
#     port
def extract_info_from_url(url):
    if is_url_relative(url): 
        relative_url = url
        hostname = None
        port = 80
    else:
        pos_start_hostname = url.find('://') + len('://')
        pos_end_hostname = url.find('/', pos_start_hostname)
        if pos_end_hostname == -1:
            relative_url = '/'
            hostname = url[pos_start_hostname:]
        else:
            relative_url = url[pos_end_hostname:]
            hostname = url[pos_start_hostname : pos_end_hostname]
        port = 80
        index_of_port_separator = hostname.find(':')
        if(':' in hostname):
            port = int(hostname[index_of_port_separator+1:])
            hostname = hostname[:index_of_port_separator]
    return {'relative_url': relative_url, 'hostname': hostname, 'port': port}


# Takes out invalid http headers and extracts the hostname
def validate_http_headers(http_headers, keep_hostname):
    valid_headers = []
    hostname = ''
    for header in http_headers:
        if header == '':
            continue
        pieces = header.split(':')
        key = pieces[0]

        if(key == 'Proxy-Connection' or 
           key == 'Connection' or 
           key == 'Upgrade-Insecure-Requests'):
            continue
        value = pieces[1]

        if key == 'Host':
            if not keep_hostname:
                continue
            else:
                hostname = value.strip()

        valid_headers.append('{key}: {value}'.format(key=key, value=value))

    return valid_headers, hostname
