PA 1 - A: Basic Proxy

To start the proxy, use the command python3 proxy.py <port_number>
For example, python3 proxy.py 6000

The proxy should accept requests in absolute and relative url format.  It works on telnet and Firefox 15.  I was able to load google.com osnews.com and arstechnica.com

