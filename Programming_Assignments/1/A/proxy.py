import sys
from socket import *


# Generates an http response error message
# given an http status code
def generate_http_error_message(number):
    error_message = 'invalid'
    if(number == 501):
        error_message = 'Not Implemented'
    elif(number == 400):
        error_message = 'Bad Request'
    return """
        HTTP/1.0 {number} {message}

        <html>
        <head>
        <title>{number} {message}</title>
        </head>
        <body>
        <h1>{} {}</h1>
        </body>
        </html>
    """.format(number=number, error_message=error_message)


# Takes out invalid http headers and extracts the hostname
def validate_http_headers(http_headers, keep_hostname):
    valid_headers = []
    hostname = ''
    for header in http_headers:
        if header == '':
            continue
        pieces = header.split(':')
        key = pieces[0]

        if key == 'Proxy-Connection' or key == 'Connection':
            continue
        value = pieces[1]

        if key == 'Host':
            if not keep_hostname:
                continue
            else:
                hostname = value.strip()

        valid_headers.append('{key}: {value}'.format(key=key, value=value))

    return valid_headers, hostname


# extracts the hostname and port from an absolute url
def extract_hostname_port_from_url(url):
    pos_start_hostname = url.find('://') + len('://')
    pos_end_hostname = url.find('/', pos_start_hostname)
    if pos_end_hostname is -1:
        hostname = url[pos_start_hostname:]
    else:
        hostname = url[pos_start_hostname : pos_end_hostname]

    port = 80
    index_of_port_separator = hostname.find(':')
    if(':' in hostname):
        port = int(hostname[index_of_port_separator:])
        hostname = hostname[:index_of_port_separator]
    return (hostname, port)


# Converts an absolute url into a relative url
def make_url_relative(url):
    pos_start_hostname = url.find('://') + len('://')
    pos_end_hostname = url.find('/', pos_start_hostname)
    if pos_end_hostname == -1:
        relative_url = '/'
    else:
        relative_url = url[pos_end_hostname:]
    return relative_url


# Determines if a url is relative
# If the url starts with a /, we say it is relative.
# We say it is false otherwise
def url_is_relative(url):
    if url.startswith('/'):
            return True
    return False


print('Starting proxy server')
serverPort = int(sys.argv[1])
serverSocket = socket(AF_INET,SOCK_STREAM)
serverSocket.bind(('0.0.0.0', serverPort))
serverSocket.listen(1)
print('Listening on port {}'.format(serverPort))
while 1:
    conn_to_client, addr = serverSocket.accept()
    print("Connected to {}".format(addr))

    # If the client or server sends something that we can't parse, an exception
    # will be thrown, this will catch it, close the client connection so another
    # client can connect
    try:
        request_from_client = ''
        # Take in the request from the client until we get a /r/n/r/n
        try:
            while '\r\n\r\n' not in request_from_client:
                request_from_client +=  conn_to_client.recv(1024).decode('ascii')
            print("Received {}".format(request_from_client))
        # On a site I visited on firefox, 
        # some of the requests contained non-ascii data, this handles that
        except UnicodeDecodeError:
            print("Unable to decode request")
            conn_to_client.send(generate_http_error_message(400))
            conn_to_client.close()
            continue

        # Extract the header info out of the header
        request_message_lines = request_from_client.splitlines()
        request_line = request_message_lines[0]
        request_line_fields = request_line.split(' ')
        http_method = request_line_fields[0]
        url = request_line_fields[1]
        relative_url = ''
        if url_is_relative(url):
            relative_url = url
            port = 80
        else:
            relative_url = make_url_relative(url)
            hostname, port = extract_hostname_port_from_url(url)
        http_version = request_line_fields[2]

        # Extract the header information that we won't use
        # (except for maybe the Host) so we can pass it on
        if len(request_message_lines) > 1:
            http_headers = request_message_lines[1:]
            if url_is_relative(url):
                valid_http_headers, hostname = validate_http_headers(
                        http_headers, True)
            else:
                valid_http_headers = validate_http_headers(
                        http_headers, False)[0]

        # Compose the request we'll send to the server
        valid_http_headers = []
        request_to_server = '{http_method} {url} HTTP/1.0\r\n'.format(
                 http_method=http_method, url=relative_url)
        request_to_server += 'Host: {host}\r\n'.format(host=hostname)
        request_to_server += 'Connection: close\r\n'
        request_to_server += '\r\n'.join(valid_http_headers)
        request_to_server += '\r\n'

        # Send the request to the server
        print("Sending to {hostname} on {port}:".format(
            hostname=hostname, port=port))
        print(request_to_server)
        conn_to_server = socket(AF_INET, SOCK_STREAM)
        conn_to_server.connect((hostname, port))
        conn_to_server.send(bytes(request_to_server, 'UTF-8'))
        print("Request sent")

        # Receive everything from the server and send it to the client
        complete_file = b''
        while 1:
            file_chunk = conn_to_server.recv(1024, MSG_WAITALL);
            complete_file += file_chunk
            if len(file_chunk) == 0:
                break

        # Close the connection to the server and send it to the client
        conn_to_server.close()
        print("Connection to server closed")
        print("Sending to client:")
        print(complete_file)
        conn_to_client.send(complete_file)
    except Exception as e:
        print("Error: {exception}".format(exception=e))

    finally:
        # Close the connection to the client
        conn_to_client.close()
        print("Connection to client closed")
