#!/usr/bin/env python3
from ClientConnection import *
import threading
import signal
import sys
from socket import *


# Exit without an exception
def sigint_handler(signal, frame):
    sys.exit(0)


def main():
    # If they try to start the program without a port, 
    # show them how to use the proxy
    if len(sys.argv) != 2:
        print('Usage: ./proxy.py port')
        return

    # Make sure the port is an integer
    try:
        serverPort = int(sys.argv[1])
    except ValueError:
        print('port needs to be an integer')
        return

    # Start listening for connections
    print('Starting proxy server')
    signal.signal(signal.SIGINT, sigint_handler)
    serverSocket = socket(AF_INET,SOCK_STREAM)
    serverSocket.bind(('0.0.0.0', serverPort))
    serverSocket.listen(1)
    print('Listening on port {}'.format(serverPort))
    while 1:
        print('Accepting connections')
        conn_to_client, addr = serverSocket.accept()
        print("Connected to {}".format(addr))

        # Create a new Client Connection to handle the requests
        client_handling_thread = ClientConnection(conn_to_client)
        client_handling_thread.start()

if  __name__ =='__main__': main()
