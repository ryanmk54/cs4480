# Generates an http response error message
# given an http status code
def generate_http_error_message(number):
    error_message = 'invalid'
    if(number == 501):
        error_message = 'Not Implemented'
    elif(number == 400):
        error_message = 'Bad Request'
    elif(number == 408):
        error_message = 'Request Timeout'
    return """
    HTTP/1.0 {number} {error_message}

    <html>
    <head>
    <title>{number} {error_message}</title>
    </head>
    <body>
    <h1>{number} {error_message}</h1>
    </body>
    </html>
    """.format(number=number, error_message=error_message)


# Generate a message that explains to the client 
# why they can't have the file they wanted
def generate_virus_message():
    return """
    HTTP/1.0 200 OK

    <html>
    <head>
    <title> Virus Found!</title>
    </head>
    <body>
    <h1>Virus Found!</h1>
    You can't download that page because cymru thinks it is a virus
    </body>
    </html>
    """


# Determines if a url is relative
# If the url starts with a /, we say it is relative.  
# Otherwise it is absolute.
def is_url_relative(url):
    if url.startswith('/'):
            return True
    return False

# returns a key consisting of the: 
#     relative url,
#     hostname,
#     port
def extract_info_from_url(url):
    # TODO curl breaks this if I request http:/localhost:8000/news.exe
    # Send back a 400 Bad Request in this case
    # TODO if it is absolute and it doesn't start with http://, prepend http://

    if is_url_relative(url): 
        relative_url = url
        hostname = None
        port = 80
    else:
        pos_start_hostname = url.find('://') + len('://')
        pos_end_hostname = url.find('/', pos_start_hostname)
        if pos_end_hostname == -1:
            relative_url = '/'
            hostname = url[pos_start_hostname:]
        else:
            relative_url = url[pos_end_hostname:]
            hostname = url[pos_start_hostname : pos_end_hostname]
        port = 80
        index_of_port_separator = hostname.find(':')
        if(':' in hostname):
            port = int(hostname[index_of_port_separator+1:])
            hostname = hostname[:index_of_port_separator]
    return {'relative_url': relative_url, 'hostname': hostname, 'port': port}


# Removes invalid headers from the given list of http headers
# If the extract_host flag is set, throw an exception if it isn't there
def remove_invalid_http_headers(http_headers, 
                                invalid_http_header_keys, 
                                extract_host):
    """
    Returns the given http_headers (and hostname if requested) 
    with the invalid headers removed
    """

    valid_headers = []
    hostname = None

    # go through each header they gave us
    for header in http_headers:
        # don't bother with empty headers
        if header == '':
            continue

        pieces = header.split(':', 1)
        key = pieces[0]
        value = pieces[1]

        # don't include invalid headers
        if key in invalid_http_header_keys:
            continue

        # don't include host, but save it for later
        if key == 'Host':
            hostname = value
            continue

        # if the header made it this far, include it
        valid_headers.append('{key}:{value}'.format(key=key, value=value))

    # if the hostname is required and not found, throw an exception
    if extract_host and hostname is None:
        raise Exception('Hostname not found when it was required')
    return valid_headers, hostname
