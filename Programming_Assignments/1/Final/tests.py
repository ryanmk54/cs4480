#!/usr/bin/env python3
import unittest
from web_utils import *

class TestStringMethods(unittest.TestCase):

    def test_extract_info_from_url(self):
        google_urls = [
            'http://google.com/',
            'http://google.com']
        for url in google_urls:
            correct_google_url_info = {
                    'relative_url': '/',
                    'hostname': 'google.com',
                    'port': 80}
            url_info = extract_info_from_url(url)
            self.assertEqual(url_info, correct_google_url_info)

        root_url = '/'
        correct_root_url_info = {
                'relative_url': '/',
                'hostname': None,
                'port': 80}
        root_url_info = extract_info_from_url(root_url)
        self.assertEqual(root_url_info, correct_root_url_info)

        kobus_absolute_url = 'http://cs.utah.edu/~kobus/simple.html'
        correct_kobus_absolute_url_info = {
                    'relative_url': '/~kobus/simple.html',
                    'hostname': 'cs.utah.edu',
                    'port': 80}
        kobus_absolute_url_info = extract_info_from_url(kobus_absolute_url)
        self.assertEqual(kobus_absolute_url_info, correct_kobus_absolute_url_info)

        correct_kobus_relative_url_info = {
                    'relative_url': '/~kobus/simple.html',
                    'hostname': None,
                    'port': 80}
        kobus_relative_url = '/~kobus/simple.html'
        kobus_relative_url_info = extract_info_from_url(kobus_relative_url)
        self.assertEqual(kobus_relative_url_info, correct_kobus_relative_url_info)

        correct_absolute_localhost_url_info = {
                    'relative_url': '/news.exe',
                    'hostname': 'localhost',
                    'port': 8000}
        localhost_absolute_url = 'http://localhost:8000/news.exe'
        localhost_relative_url_info = extract_info_from_url(localhost_absolute_url)
        self.assertEqual(localhost_relative_url_info, correct_absolute_localhost_url_info)


    def test_validate_http_headers(self):
        # If the http headers are invalid, throw an exception
        raise NotImplementedError("Still need to test improperly formatted "
                "headers.")

        # HTTP headers should not include:  
        # Proxy-Connection, Connection, or Upgrade-Insecure-Requests
        raise NotImplementedError("Still need to verify http headers don't"
                " include Proxy-Connection, Connection, "
                " or Upgrade-Insecure-Requests")

        # Verify Hostname is taken out if keep_hostname is not set
        raise NotImplementedError("Still need to verify hostname is taken out "
                "if keep_hostname is not set")

        # Verify Hostname is left in if keep_hostname is set
        raise NotImplementedError("Still need to verify hostname is left in if "
                " keep_hostname is set")


    def test_remove_invalid_http_headers(self):
        # If the http headers are invalid, throw an exception
        raise NotImplementedError("Still need to test improperly formatted "
                "headers.")

        # Verify that invalid keys are taken out with one invalid key
        raise NotImplementedError("Still need to verify invalid keys are taken "
                "out with one invalid key")

        # Verify that invalid keys are taken out with multiple invalid keys
        raise NotImplementedError("Still need to verify invalid keys are taken "
                " out with multiple invalid keys")

        # Verify that invalid keys are taken out with only one header
        raise NotImplementedError("Still need to verify invalid keys are taken "
                " out with one header")

        # Verify that an exception is thrown when a host is not found 
        # and it should be
        raise NotImplementedError("Still need to verify an exception is thrown "
                " when the host is not found and we requested it")
if __name__ == '__main__':
    unittest.main()
