import hashlib
import linecache
import threading
from socket import *
import subprocess
from subprocess import call, check_output
import sys
from web_utils import *


class ClientConnection(threading.Thread):
    def send_file_to_client_and_close_connection(self, bytes_message):
        # Send a message to the client and close the connection
        self.conn_to_client.send(bytes_message)
        self.conn_to_client.close()
        print("Connection to client closed")
        return

    def __init__(self, conn_to_client):
        threading.Thread.__init__(self)
        self.conn_to_client = conn_to_client
        request_from_client = b''

        # Take in the request from the client until we get a /r/n/r/n
        while b'\r\n\r\n' not in request_from_client:
            request_as_bytes = conn_to_client.recv(1024)
            if not request_as_bytes:
                print("Closing conn")
                self.conn_to_client.close()
                return
            request_from_client +=  request_as_bytes

        # Decode the request, to make it easier to parse
        request_from_client = request_from_client.split(b'\r\n\r\n', 1)[0]
        print(request_from_client)
        request_from_client = request_from_client.decode('ascii')

        # Extract the header info out of the header
        request_message_lines = request_from_client.splitlines()
        request_line = request_message_lines[0]
        request_line_fields = request_line.split(' ')
        http_method = request_line_fields[0]
        url = request_line_fields[1]

        if http_method != 'GET':
            self.send_file_to_client_and_close_connection(
                    bytes(generate_http_error_message(501), 'ascii'))
            print("Connection to client closed because there was no get method")
            return

        try:
            url_info = extract_info_from_url(url)
            relative_url = url_info['relative_url']
            hostname = url_info['hostname']
            port = url_info['port']
            http_version = request_line_fields[2]
        except Exception as e:
            print("Error while trying to parse the url_info")
            self.send_file_to_client_and_close_connection(
                    bytes(generate_http_error_message(400), 'ascii'))
            return

        valid_http_headers = []
        # Extract the header information that we won't use
        # (except for maybe the Host) so we can pass it on
        if len(request_message_lines) > 1:
            http_headers = request_message_lines[1:]
            invalid_http_header_keys = ['Proxy-Connection', 'Connection',
                    'Upgrade-Insecure-Requests', 'Accept-Encoding']
            if is_url_relative(url):
                # TODO if the url is relative and the headers don't have a
                # hostname, send back a 400 Bad Request
                try:
                    valid_http_headers, hostname = remove_invalid_http_headers(
                            http_headers, invalid_http_header_keys, True)
                except Exception as e:
                    print("Error while validating http headers")
                    self.send_file_to_client_and_close_connection(
                            bytes(generate_http_error_message(400), 'ascii'))
                    return
            else:
                valid_http_headers = remove_invalid_http_headers(
                        http_headers, invalid_http_header_keys, False)[0]

        # Compose the request we'll send to the server
        request_to_server = '{http_method} {url} HTTP/1.0\r\n'.format(
                 http_method=http_method, url=relative_url)
        request_to_server += 'Host: {host}\r\n'.format(host=hostname)
        request_to_server += 'Connection: close\r\n'
        request_to_server += '\r\n'.join(valid_http_headers)
        request_to_server += '\r\n\r\n'

        # Send the request to the server
        print("Sending to {hostname} on {port}:".format(
            hostname=hostname, port=port))
        print(request_to_server)
        conn_to_server = socket(AF_INET, SOCK_STREAM)

        # If a bad hostname goes in, catch the exception.
        try:
            conn_to_server.connect((hostname, port))
            conn_to_server.send(bytes(request_to_server, 'UTF-8'))
        except timeout as e:
            print('A Timeout occurred while trying to connect to the server')
            print(e.strerror)
            send_file_to_client_and_close_connection(
                    bytes(generate_http_error_message(408), 'ascii'))
            conn_to_server.close()
            print("Connection to server closed")
            return
        # TODO gaierror and OSError have exactly the same code.
        # These could be combined into one
        except gaierror as e:
            print('Unable to connect to server:')
            print(e.strerror)
            self.send_file_to_client_and_close_connection(
                    bytes(generate_http_error_message(400), 'ascii'))
            conn_to_server.close()
            print("Connection to server closed")
            return
        except OSError as e:
            print('Unable to connect to server: {}')
            print(e.strerror)
            self.send_file_to_client_and_close_connection(
                    bytes(generate_http_error_message(400), 'ascii'))
            conn_to_server.close()
            print("Connection to server closed")
            return
        print("Request sent to server")

        # Receive everything from the server
        complete_file = b''
        while 1:
            file_chunk = conn_to_server.recv(1024, MSG_WAITALL);
            complete_file += file_chunk
            if len(file_chunk) == 0:
                break

        # Close the connection to the server and send it to the client
        conn_to_server.close()
        print("Connection to server closed")

        # Test the file against cymru
        split_file = complete_file.split(b'\r\n\r\n', 1)
        file_header = split_file[0]
        print('File header is:')
        print(file_header)
        file_to_send_to_cymru = split_file[1]
        hash = hashlib.md5(file_to_send_to_cymru).hexdigest()
        print('Hash of this file is: {hash}'.format(hash=hash))

        # If there is file_to_send_to_cymru is empty, all I should send is the
        # header.  The header doesn't contain a virus
        if not file_to_send_to_cymru:
            print("Sending file to client:")
            self.send_file_to_client_and_close_connection(complete_file)
            return
        try:
            whois_output = check_output(['whois', '-h', 'hash.cymru.com', hash])
        except subprocess.CalledProcessError as e:
            print('Unable to check if the file is a virus:')
            send_file_to_client_and_close_connection(
                    bytes(generate_http_error_message(408), 'ascii'))
            conn_to_server.close()
            print("Connection to server closed")
            return

        if whois_output.endswith(b'NO_DATA\n'):
            # no virus, send the file
            print("Sending file to client:")
            self.send_file_to_client_and_close_connection(complete_file)
            return
        else:
            # virus, send an error message
            print("File had a virus, sending error to client")
            client_message = generate_virus_message()
            client_file = bytes(client_message, 'ascii')
            self.send_file_to_client_and_close_connection(client_file)
            return
