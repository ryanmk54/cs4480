#! /usr/bin/env python3
from socket import *

serverName = 'localhost'
serverPort = 6000
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName, serverPort))
sentence = input('Input:')
clientSocket.send(bytes(sentence, 'UTF-8'))
modifiedSentence = clientSocket.recv(1024)
print('From Server:', modifiedSentence.decode('UTF-8'))
clientSocket.close()
