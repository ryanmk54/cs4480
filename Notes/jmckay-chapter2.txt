2.1

2.1.1*

- Be able to explain and compare the two predominant application architectures
in use in the Internet, including giving examples and discussing the pros and
cons of each approach.

The two predominant application architectures in use in the Internet is the client-server architecture and
the peer-to-peer architecture. 

2.1.2*

- Be able to explain how networked applications are realized and communicate.

Network applications have processes that communicate with other processes on different hosts.
They communicated by sending messages onto the network and receive messages from the network.

- Be able to explain the API provided for communication between application
processes and the network.

The API provided for communication between application processes and the network is a socket.
A socket interfaces the application layer to the transport layer. An application gives a message
to the socket and the socket sends it off onto the network.


2.1.3

- Be able to list and describe the different dimensions along which
transport services can be provided to applications.

There are 4 possible services that a transport layer can offer, reliable data transfer, 
throughput, timing, and security.

reliable data transfer - guarantees that what ever the client sends will get to the receiving host, without losses, or corruption.
througput - throughput transport service guarantees that there will be a rate r bits/sec of throughput always available.
timing - timing transport services guarantee that a message will get to the receiving host no more than a specific set time.
security - security transport services can encrypt all data transmitted by the sending process, and in the receiving host, the
transport-layer protocol can decrypt the data before delivering it to the receiving process. Other features it might have include data
integrity and end-point authentication.   

- Be able to give examples of applications that would require and/or
benefit from transport services being provided along these different
dimensions.

For reliable data transfer, banks, and other financial applications would need to require reliable data transfer.
For throughput, different internet telephony applications might require or benefit to have the necessary bandwith for transmitting r bits/sec
For timing, multimedia applications might use this for real time such as games or video or internet telephony
For security, banks and credit card, or personal information applications would probably require this to protect sensitive information.

2.1.4

- Be able to describe the transport services that are and are not provided
by the Internet.

The transport services that are provided  by the internet are reliable data transfer and security.
The transport services that are not provided by the internet currently are timing, and throughput.

- Provide examples of applications that use the different Internet transport
services.

Examples that use reliable data transfer include, Email, the web HTTP, file transfer.
Examples that use security are banks, financial applications, etc.

2.1.5*

- Be able to explain the purpose of application layer protocols and the
relationship between applications and application layer protocols.

The purpose of an application-layer protocol is to provide a way for an application to be able
to communicate with any other application using that same protocol.

The application-layer protocol specifies the rules, format, and instructions for communicating
between different application proccesses running on separate hosts. 

2.2

2.2.1

- Be able to explain the working of the HTTP protocol including the application
architecture being used, the type of interaction that is used, the transport
protocol used and the characteristic that allows the protocol to scale.

HTTP uses the client server architecture, the client sends an http request to the server
and the server sends a reply. HTTP uses tcp as its transport protocol.
HTTP is able to scale because it is stateless meaning it doesnt have to keep track of previous request.

2.2.2*

- Be able to explain and reason about the details of downloading and rendering
in a browser a typical web page with embedded objects assuming both non-persistent
and persistent connections. 

When downloading and rendering in a browser with embedded objects and using a non-persistent connection, then 
when you first click on the website link your browser sets up a tcp connection and starts the 3 way handshake.
First you send a request to connect message, then the server replies with an accept message, then you reply with
an acknoledgemnt message and at the same time include your get request. Then the server will send you the html webpage
you requested and the server will tell tcp to close the connection socket. Tcp wont actually close it untill it gets a
confirmation from the client that it received the full message. Then the client receives the servers message and it closes
the tcp connection. If the webpage that was request has text and 10 jpeg images on it, then it will have to create 10 additional
tcp connections to get the jpeg images, because the text was already received in the first connection. This makes it so you have
2 RTTs (Round Trip Time) delay per request.
 
When downloading and rendering in a browser with embedded objects and using a persistent connection, once you setup the first connection and
do the first http request, and the server sends the html web page to you then you still keep the tcp connection and you are able to then 
make additional requests on that same connection. This makes it so each request takes just 1 RTT delay.

2.2.3

- Be able to identify and describe the purpose of the elements
of an HTTP request. 

An http request can be broken up into a request line, zero or more header lines, and an entity body (payload/data).

The different elements of the request line include the request method, url, and version.
The request method tells what kind of request the browser/client is making, such as GET, POST, HEAD.
The url specifies what the client is requesting, it can contain the host and/or the path to the requested object.
The version specifies what version of HTTP the browser/client is implementing, 1.0, 1.1.

The header lines are used to provide additional functions or features.
Some common headers are:
Host: is used alot in web proxy caches
Connection: close, tells the server to close the connection after sending the requested object

The entity body is where data is stored that is included the request the client sends to the server.
The entity body is empty for a GET request, but is used for a POST request, in the case where the user fills out a form
and submits it to the server.

- Be able to explain the need for and use of different HTTP methods fields.

The different HTTP methods discussed in the book include:
GET: used for requesting an object from the server, where the requested object is identified in the url field.
POST: In a POST message,the user is still requesting a web page from the server, but the specific contents of the web page
depend on what the user enters into the fields.
HEAD: head is similar to GET, when a server receives a head request it responds with an HTTP message but it leaves out the request object.
Application developers often use the HEAD method for debugging.
PUT: often used in conjunction with web publishing, it allows a user to upload an object to a specific path/directory on a specific web server.
DELETE: allows a user or an application to delete an object on a web server.

- Be able to identify and describe the purpose of the elements
of an HTTP response.

An http response will include a status line, header lines, and an entity body containing the object.
Status line has three fields: protocol version field, status code, and status message
Header lines can be used to provide helpful information, additional functionality and features.
The Connection close: tells the client that the server is going to close the connection after sending the data

- Be able to interpret, analyze and explain a transcript of
HTTP interaction.

I can interpret, analyze, and explain a transcript of an HTTP interaction. Can you?

2.2.4**

- Be able to explain the need for HTTP cookies.

Since HTTP server is stateless, it doesnt natively keep track of users that connect to it.
Being able to identify a user may be desireable especially for large businesses such as amazon or ebay
to provide content specific to the user.
To overcome this HTTP uses cookies.

- Be able to explain how HTTP cookies satisfy this need.

The cookie technology uses 4 components. 
1. A cookie header line in the HTTP response message.
2. A cookie header line in the HTTP request message.
3. A cookie file kept on the user's end system and  managed by the user's browser.
4. A back-end database at the web site.

When a user connects to amazon for the first time, amazon takes their ip information and generates a unique
identification number. Then the amazon server responds to the user's browser, including in the HTTP response
a Set-cookie: header, which contains the identification number, e.g. Set-cookie: 1678.
The the user's browser receives the HTTP response message, it sees the Set-cookie: header and then it appends
a line to the special cookie file that it manages. This line includes the hostname of the server and the identification
number in the Set-cookie: header. As the user continues to browse amazon, each time they request a web page, her browser
consults the cookie file, extracts the identification number for amazon, and puts a cookie header line that includes the
identification number in the HTTP request, Cookie: 1678. This allow's the amazon server to track the user's activity at the
amazon website and can be used to keep track of a shopping cart, or display adds that are similar to the users activity, etc.


2.2.5*

- Be able to explain the functionality of an HTTP proxy server.

An HTTP proxy can be used to satisfy HTTP requests on behalf of a client.
The proxy can cache client requests for quick access later.

The way it works, is a client makes a tcp connection to a proxy and then it sends its http requests for an origin server to the proxy.
The proxy then checks to see if it already has a cached copy of the client's request and if so immediately replies to the client and closes
the connection. If the client's request is not cached, it makes a tcp connection with the origin server and sends the clients request to the server.
The server then sends the response and after the proxy receives the response it closes the connection to the server and it makes a cache
copy of it and then it sends it to the client over the existing tcp connection between the client and proxy.

- Be able to explain different scenarios in which HTTP proxies could
be useful.

One scenario for a proxy server would be the programming assignment we did. Where we made a proxy that performed
maleware checks on requested data from a client to an orgin server. 
Another scenario would be if a university used a proxy for all of its outgoing internet HTTP requests, and was able
to cache most of the requests and reply instantly to the clients. 

2.2.6*

- Be able to explain the HTTP conditional GET mechanism and explain where
it could/should be used.

The conditional GET mechanism is used to keep cached objects for a proxy up to date.
Where it could/should be used is in the case where a client makes an HTTP request and the proxy makes the request
to the origin server and when it gets the servers respone it sends the response back to the client but also makes 
a copy of the object locally in its cache. It also copies the Last-Modified: header with the object. Then when the client
comes back a week later and requests the object again the proxy will see that it has that object in its cache, but since
the object on the origin server could have been modified in the past week the proxy performs an up to date check by issueing
a conditional GET. Where it adds the header If-modified-since: and it attaches the date from when it last save the object.
This tells the server to send the object only if the object has been modified since the specified date. If it has not been modified
the server still sends a response message to the proxy but does not include the requested object in the response message. The status
line may also say HTTP/1.1 304 Not Modified, which tells the proxy it can go ahead and forward the proxy's cached copy of the requested
object.

2.3

- Be able to explain at least three ways in which FTP
differs from HTTP (from a protocol perspective, not a user
perspective).

FTP uses two parallel TCP connections to transfer a file, a control connection, a data connection.
FTP uses the control connection to send control information between the two hosts, such as user identification, password, commands to
change remote directory, and commands to "put", and "get" files.
FTP uses the data connection to actually send a file. 
FTP data connections are non-persistent, meaning that a new data connection has to be made for each file.
FTP keeps track of state. It has to keep track of the user accound and it has to keep track of the current
directory the user is currently in to navigate between directories.

2.4*

- Be able to list and describe the functionality of the three
main components that enable email.

The three main components that enable email are user agents, mail servers, and SMTP (simple mail transfer protocol).

User agents allow the user to read, write, attach, save and compose messages.

Mail servers form the core of the email infrastructure. Mail servers contain mail boxes for each email account.
When a user agent sends an email it goes first to its mail server, where it will try to reach the recipient's mail server.
Once it reaches the recipient's mail server, the recipient's mail server puts it in the recipient's mail box. Then the recipient
logs in to their user agent and reads it. Mail servers also have a message queue that they store the email until they are able to
succesfully send it to the recipients mail server. It will try to resend on intervals of 30 min for a few days before it removes the
message and notifies the sender.

SMTP, simple mail transfer protocol is the transfer protocol for electronic mail. It uses TCP for reliable data transfer.
SMTP has two sides, a client side, which executes on the sender's email server and a server side which executes on the 
recipient's mail server. Both the client and server sides of SMTP run on every mail server.

2.4.1* 

- Be able to explain the steps involved for Alice to send email to Bob
whose email account is associated with a different mail server than
that of Alice.

First Alice opens up her user agent and she enters bob's email address and composes a message.
Alice's user agent then sends the message to alice's mail server. The message gets stored on a queue and
the client side of alice's mail server will see it in the queue and it will try to setup a tcp connection with bob's mail server.
If it is able to setup a tcp connection with bob's mail server, it will send the message to bob's mail server. Then bob's mail server
will put it in bob's mail box on the mail server. Then bob can use his user agent to access the message.
If Alice's mail server is not able to contact bob's mail server, it will retry to find it and send the message about every 30 min for 
a few days before it will drop it and send Alice a message saying it couldnt send it.

- Be able to interpret, analyze and explain a transcript of an SMTP exchange
between an SMTP client and server.

yes i can, can you?

2.4.2

- Be able to list and describe three differences between SMTP and HTTP
(from a protocol perspective).

1. HTTP is mainly a pull protocol, meaning the client is requesting something from the server, but 
SMTP is mainly a push protocol, meaning the client is sending something to the server.

2. SMTP requires that everything sent ,the message and the body, be in 7-bit ASCII format, whether its text, an image, or media files.
 HTTP data does not impose this restriction.
 
3. HTTP encapsulates each object in its own HTTP response message, while SMTP places all of themessage's objects into one message. 

2.4.3

- Be able to explain the relationship between message header lines and
SMTP commands.

Message header lines are part of the mail message itself, such as
From: alice@crepes.fr
To: bob@hamburger.edu
Subject: Searching for the meaning of life.

The SMTP commands are for the tcp handshing protocol.

2.4.4

- Be able to explain the need for mail access protocols and how they
are different from SMTP.

SMTP is only a push protocol meaning that it only sends messages and SMTP is used to transfer mail
from the sender's mail server to the recipient's mail server. SMTP is also used to transfer mail from
the sender's user agent to the sender's mail server.

This means that recipient can't use SMTP to pull their messages from their mail server to their user agent,
because that would be a pull operation and smtp is a push protocol.

To overcome this a special mail access protocol is used to transfer messages from the recipient's mail server to his
local pc, or device where his user agent is being used. 
Some mail access-protocols are post office protocl version 3 (POP3), Internet Mail Access Protocol (IMAP), and HTTP.

- Be able to explain the difference between POP3 and IMAP.

With a tcp connection established pop3 goes through 3 phases: authorization, transaction, update.
Authorization i where the user is authenticated. Transaction is where the user retrieves messages, and can mark things to be 
deleted, or remove deletion marks, and obtain mail statistics. Update occurs after the user issues the quit command ending the 
pop3 session; at this time, the mail server deletes the messages that were marked for deletion.
The transaction phase can be configured to "download and delete" or "download and keep". 
Download and delete lets the user download the message to their device and it is deleted from the mail server and the only copy is
on the device. This doesn't allow multiple devices to read a message because it wont be on the mail server if another device downloaded it
and deleted it. In the Download and keep configuration, the user's device downloads it but a copy of it stays on the mail server where
it can be accessed later on another device.
POP3 maintains some state in particular it keeps track of which user messages were marked as deleted so it can delete when the update phase
occurs. POP3 does not keep state across different POP3 sessions, once you quit the session its gone.

An IMAP server will associate each message with a folder, when a message first arrives at the server, it is associated
with the recipient's Inbox folder. The recipient can then move the message into a new, user-created folder, where they can
read it, delete it, and so on.
IMAP protocol provides commands to allow users to create folders and move messages from one folder to another.
It also provies commands that allow users to search remote folders for messages matching specific criteria.
Unlike POP3, an IMAP server maintains user state information across IMAP sessions, for example the names of folders and which messages
are in them.
Another important feature of IMAP is that it has commands that permit a user agent to obtain components of messages, like just the 
message header or just one part of the multipart MIME message. This is useful when bandwidth is limited and the user doesnt want to download all of the messages in the mailbox, avoiding long messages 


2.5**

- Be able to explain the need for the domain name system.

Internet hosts can be identified by their hostname or their IP address.
Humans like to use mneumonic hostname identifiers such as www.google.com, but machines such as routers like to use the
fixed lenght, hierarchically structured IP addresses. In order to reconcile these preferences, we need a directory service
that translates hostnames to IP addresses. This is the main task of the Internet's domain name system (DNS).

2.5.1**

- Be able to explain how DNS, which is an application level protocol,
is not really used as an application by itself, but all the same is
an essential part of nearly every application on the Internet.
(Part of explanation should involve a step-by-step explanation of
how another protocol interacts with and uses DNS.)

DNS is commonly employed by other application-layer protocols- including HTTP, SMTP, and FTP, to translate user-supplied
hotnames to IP addresses.
In order for the user's host to be able to send an HTTP request message to a web server, the user's host must first obtain
the IP address of the server. 
This is done as follows.
1. The same user machine runs the client side of the DNS application.
2. The browser extracts the hostname, from the url and passes the hostname to the client side of the DNS application.
3. The DNS client sends a query containing the hostname to a DNS server.
4. The DNS client eventually receives a reply, which includes the IP address for the hostname.
5. Once the browser receives the IP address from DNS, it can initiate a TCP connection to the HTTP server process located
at port 80 at that IP address.

- Be able to list and describe four different services provided by DNS.

The four different services provided by DNS include: Host to IP translation, Host aliasing, Mail server aliasing, and Load distribution.

Host to IP translation, is used to translate mneumonic hostnames that are easily remembered for humans to an numeric, hiarchichal ip address
that routers and machines use to communicating.

Host aliasing is used for making complicated canonical hostname such as relay1.west-coast.enterprise.com to a more simple mneuominc hostname such as
enterprise.com

Mail server aliasing is used to make canoncial email addresses more mneumonic such as relay1.west-coast.hotmail.com to hotmail.com.
DNS can be invoked by a mail application to obtain the canonical hostname for a supplied alias hostname as well as the IP address of the host.

For Load distribution, when a client makes a dns query for a hostname that has multiple IPs the DNS will return the list and rotate the order
of the list each time it gets a request for that same host. Usually the client goes to the first ip in the list and by dns rotating the list
helps distribute the amount of traffic to the different IPs for a host.

2.5.2**

- Be able to describe the DNS hierarchy and explain the functionality provided
by each level in the hierarchy. (Include the local DNS server as part
of the hierarchy.)

The dns hierarchy has root DNS servers at the top, then undernieth the root DNS servers, there are the Top level domain (TLD) dns servers,
then below the top level domain servers is the authoratative dns servers. However since we are asked to include the local DNS server in the
hierarchy then I think it would go above the root dns server since the client sends it dns queries directly to the local dns server.

The local DNS server is similar to a proxy in that it makes dns query requests on behalf of the client.
The root dns server keeps track of the ip addresses to all the top level domain servers.
The top level domain servers keep track of all the ip addresses for authoratative dns servers under a specific domain, such as com, edu, etc.
The authorative dns server keeps track of all the ip addresses for an organization such as amazon.

- Be able to explain the interactions and steps involved in performing a
DNS lookup.

In performing a dns lookup, the client application first takes the target host and make a dns query to its local dns server.
The local dns server then forwards the dns query to the root DNS server. the root dns server reads the domain in the host, which could be
com, or edu, and it returns to the local DNS server a list of IP addresses for TLD servers responsible for that domain.
The local DNS server then resends the query message to one of these TLD servers. The TLD server takes note of second domain section
for an example, umass.edu suffix in the url gaia.cs.umass.edu, and responds with IP address of the authoritative DNS server
for the University of Massachusetts, namely, dns.umass.edu.
Finally, the local DNS server resends the query message directly to dns.umass.edu, which responds with the IP address of
gaia.cs.umass.edu. Then the local DNS server responds to the client with the ip address for the requested host.

2.5.3**

- Be able to interpret, analyze and explain the output from common DNS tools
such as nslook and dig.

I think i can, can you?

- Be able to explain the process whereby DNS entries for a new domain
name is entered into the DNS system.

First you need to register the domain name at a registrar. A registrar is a commercial entity that verifies the uniqueness of
the domain name. They enter the domain name into the DNS database, and collects a small fee from you for its services.
When you register the domain name with some registrar, you also need to provide the registrar with the names and IP addresses 
of your primary and secondary authoritative DNS servers. 

- Be able to explain a number of ways in which DNS is vulnerable to attack.
(Excluding flooding attacks.)

A man in the middle attack, where the attacker intercepts queries from hosts and returns bbogus replies. 
In DNS poisoning attack, the attacker sends bogus replies to a DNS server, tricking the server into accepting bogus records into its cache.
Exploit the DNS infrastructure to launch a DDosS attack against a targeted host. In this attack, the attacker sends DNS queries to many 
authoritative DNS servers, with each query having the spoofed source address of the targeted host. The DNS servers then send their replies
directly to the targeted host. If the queries can be crafted in such a way that a response is much larger (in bytes) than a query (so-called
amplification), then the attacker can potentially over-whelm the target without having to generate much of its own traffic.

2.6

2.6.1*

- Be able to explain the inherent scalability benefit that P2P 
has (assuming the ideal conditions explained in this section).

The reason P2P is scalable is because once the server distributes 1 complete copy of a file into the internet, then a peer that has the copy
can start redistributing that copy to other peers in chunks. By doing this is makes it so if you have N peers, the server doesnt have to
send the file N times, which would create a lot of work on the server, instead it may even send it only once and the peers will distribute it
amongst them selves and so on and as the number of peers increase the more people that can help redistribute the file.

For a client server architecture the server has to send a copy of the file to N number of clients, and as N increases the distribution time
increases linearly without bound.

You might be asked to provide an intuitive explanation in essay style.
Alternatively you might be asked to do this by explaining the
meaning of each of the terms in equation 2.1 and equation 2.2 as part
of your explanation.

For equation 2.1 and equation 2.2 in the book on pages 146, and 147, I will give an intuitive explanation for them.

Equation 2.1 is used for the client-server architecture and it is used to calculate the total distribution time for
every client to get a copy of a file from the server. 
The server must transmit one copy of the file to each of the N peers. The file size is F bits. Thus the server must transmit NF bits.
If I have 3 clients, I have to send 3 copies of the file which is 3*F. 
Since the server's upload rate is U, the time to distribute the file must be at least the total number of peers times the total bits
in the file divided by the server's upload rate. This gives us the total time to distribute a complete copy to all N peers.
We also need to account for the download rate of the peer with the slowest download rate. The peer with the slowest download rate,
cannot obtain all F bits of the file in less than F/download rate. Thus the minimum distribution time can the number of bits in the file
divided by the slowest download rate. 
Thus the total distribution time can be determined by taking the maximum time between the time it takes the slowest peer to get the file and
the time it takes the server to upload a copy of the file to all N peers. 

Equation 2.2 is used for the peer-to-peer architecture and it is used to calculate the total distribution time for every peer to get a copy
of the file that the server has. For this architecture at the beginning of the distribution only the server has the file. So we know that the
minimum time is at least the time it takes for the server to send the file once. So the minimum distribution time is at least the number of
bits divided by the upload rate of the server for one file. Again like in the client server architecutre, we need to consider the time it takes
the peer with the slowest download rate to receive the file. Thus we know the total distribution time could be the time it takes the slowest
peer to download the file which is the number of bits divided by the slowest peer's download rate. 
One thing to consider, once the server sends out the first copy of the file, it can be redistributed to other peers by the peer that
received it. Thus the total upload capacity of the system as a whole is equal to the upload rate of the server plus the upload rates of each
of the individual peers, meaning you come up with a total upload rate for the system, which is a summation of the server's upload rate and 
each of the peer's upload rate. With that we know if there are N peers in the system, and there are F bits in the file. The total distribution
time for the system, is the number of peers times the number of bits in the file divided by the total system's upload rate.

- Be able to explain the functioning of the BitTorrent P2P protocol.

The way BitTorrent works is peers in a torrent download equal-size chunks of a file from eachother, with a typical chunk size of 256KBytes.
When a peer first joins a torrent, it has no chunks. Overtime it accumulates more and more chunks. While it downloads chunks it also uploads
chunks to other peers. Once a peer has acquired an entire file, it may (selfishly) leave the torrent, or (altruistically) remain in the torrent
and continue to upload chunks to other peers. Also any peer may leave the torrent at any time with only a subset of chunks, and later 
rejoin the torrent. 

2.6.2**

- Be able to explain how a distributed hash table might be implemented
to realize a key-value pair database in P2P fashion. Including DHT
construction, storing and retrieving entries.




- Be able to explain how a circular DHT could be used to allow
a DHT to scale. (Be able to articulate the scaling problem that
a circular DHT is meant to solve.)



- Be able to explain problems with a "purely circular" DHT, how that
might be solved. Be able to articulate the generic tradeoff 
in DHT construction.



- Be able to explain how DHTs manage to maintain both the DHT and
information that it contains despite peer churn.

2.7*

- From a socket programming perspective, be able to explain the
similarities and differences between implementing a client/server
application implemented with UDP versus TCP as the underlying transport
protocol.
