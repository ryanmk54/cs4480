Note: With "reason about" I mean understand something well enough
that you can argue its pros and cons, not just give a definition.
Also, where applicable, "reason about" would include reproducing and using
the symbolic definitions (or equations) you have used in the homework
problems.

1.1

1.1.1

- Know the terminology of components and role-players that make up the Internet.

1.1.2

- Understand and be able to explain concepts behind distributed 
applications and APIs as it relates to the Internet.

1.1.3*

- Be able to define what is meant with a network protocol.
- Be able to design and explain network protocols to achieve
certain functionality using a ladder diagram. (Like in Figure 1.2).

1.2

1.2.1

- Be able to explain the basic working of and differences between
different access network technologies. Including typical up- and
down-link speeds for each.

- Be able to explain the basic workings of enterprise and home
networking technologies. Including typical link speeds.

1.2.2

- Be able to describe the workings and tradeoff of different 
physical media.

1.3

1.3.1*

- Be able to explain the devices and mechanisms involved in packet
switched networks. 
- Be able to reason about the transmission delay experienced in a packet switched
network based on the packet length and transmission rate.
- Be able to explain why queueing delay and packet loss might occur in a
packet switched network.  
- Be able to explain the relationship between forwarding and routing
in a packet switched network.

1.3.2

- Be able to explain the workings of a circuit switched network.
- Be able to explain how frequency-devision multiplexing and time-devision
multiplexing relate to circuit switched networks.
- Be able to explain the tradeoffs between packet and circuit switched
networks and the conditions under which either approach would be favored
over the other.

1.3.3**

- Be able to explain the structure and role players involved in the global
Internet.

1.4**

1.4.1**

- Be able to define and reason about the different kinds of delay that can be
experienced in packet switched network.
- Be able to explain under what conditions the relative contribution
of different delay components would be more prominent.

1.4.2**

- Be able to define traffic intensity and its relation to queueing delay.

1.4.3**

- Be able to express and reason about the end-to-end delay as a function of
the individual nodal delays.
- Be able to explain how the traceroute tool output is produced (under the hood)
and be able to interpret the output of a traceroute run.

1.4.4**

- Be able to explain and reason about the concepts of throughput and bottleneck
links in computer networks. 

1.5

1.5.1*

- Be able to explain the concept of a layered architecture in computer networks.
- Be able to list and describe the basic functionality of the layers
in the Internet protocol stack.

1.5.2*

- Be able to explain the concept of encapsulation and how it applies in the
Internet layered architecture.

1.6*

- Be able to explain examples of how bad guys might attack endpoints
and the network and the communication it enables.

1.7

- Know the number and location of the first nodes of the ARPAnet and the
first function that was performed and its result. (And take heart…)

