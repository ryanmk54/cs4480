5.1

5.1.1

- Be able to list and explain possible services that the link layer may provide.

5.1.2*

- Be able to explain where/how in a computer system the link layer is
implemented.

5.2

- Be able to explain the purpose of error-detection and error-correction techniques.

5.2.1

- Be able to explain the function of parity (including single bit
and two-dimensional party schemes).

5.2.3

- Be able to explain the basic approach of how CRC works.

5.3

- Be able to articulate the need for multiple access protocols.
- Be able to list the three basic categories of multiple access
protocols.

5.3.1*

- Be able to explain three forms of channel partitioning protocols.

5.3.2*

- Be able to explain the operation of a CSMA/CD access protocol.
- Be able to explain the  purpose of the binary exponential backoff
algorithm used in Ethernet.

5.3.3*

- Be able to name and explain two types of taking-turns protocol.

5.4

5.4.1**

- Be able to explain where in a LAN environment one might find MAC addresses.
- Be able to explain why networked devices need to have MAC addresses (when
they already have IP addresses).
- Be able to explain the purpose of the address resolution protocol
and where it is used.
- Be able to explain how the ARP protocol works in a switch environment.
- Be able to explain how the ARP protocol works in a combination switch
and router environment.

5.4.2*

- Be able to explain the purpose of the fields in the Ethernet frame structure.
(Assume the frame structure will be provided.)
- Be able to explain why in modern Ethernet environments there is no real
need for a MAC protocol.

5.4.3**

- Be able to explain "filtering" and "forwarding" in the context of link-layer
switches.
- Explain the self-learning property of link-;ayer switches.
- Be able to list and explain three benefits of switches (compared
to hubs).
- Be able to reason about the benefits and tradeoffs between switches and
routers.

5.4.4*

- Be able to explain the concept of VLANs.
- Be able to explain the concept of VLAN trunking and where that might be used.

5.5

5.5.1*

- Be able to explain why we might think of MPLS as a blending of
virtual circuit and datagram forwarding paradigms.
- Be able to explain how forwarding in a label-switched router
is done.
- Be able to suggest possible labels for the forwarding tables of a
a small MPLS network.
- Be able to explain how MPLS labels are chosen and distributed in 
a real network.
- Be able to name and explain at least two benefits (or use cases) for MPLS
networks.

5.7**

- Be able to explain the steps and protocols involved with the scenario 
where a laptop is connected to a wired network and the user starts up
a browser to download a web page.

